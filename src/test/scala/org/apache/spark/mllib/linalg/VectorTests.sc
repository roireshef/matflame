import breeze.linalg.{CSCMatrix, Matrix, max}
import org.apache.spark.mllib.linalg.{DenseMatrix, Matrices, Matrix, SparseMatrix}
import java.util.Random

import org.apache.commons.math.linear.BlockFieldMatrix
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.mllib.linalg.MatrixUtils._
import org.apache.spark.mllib.linalg.distributed.{BlockMatrix, CoordinateMatrix, ExtendedBlockMatrix, MatrixEntry}

val x = Matrices.rand(5,5,new Random(123)).asInstanceOf[DenseMatrix] //.toBreeze
val y = Matrices.rand(5,5,new Random(234)).asInstanceOf[DenseMatrix] //.toBreeze

println(x.toString(1000,1000))
println(y.toString(1000,1000))

/*
def parallelize(x: DenseMatrix) ={
  val entriesLocal = for (row <- 1 until x.numRows;col<-0 until x.numCols) yield MatrixEntry(row,col,x(row,col))
  val block =
    new CoordinateMatrix(SparkContext.getOrCreate(new SparkConf().setMaster("localhost[2]").setAppName("stam")).parallelize(entriesLocal), x.numRows, x.numCols).
      toBlockMatrix()
}

val xBlock = parallelize(x)
val yBlock = parallelize(y)

val xEBM = ExtendedBlockMatrix(xBlock)
val yEBM = ExtendedBlockMatrix(yBlock)

println(xEBM.toLocalMatrix().toString())*/
