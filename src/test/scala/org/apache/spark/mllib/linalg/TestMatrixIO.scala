package org.apache.spark.mllib.linalg

import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.SparkTest
import org.apache.spark.ml.linalg.Implicits._
import org.apache.spark.mllib.linalg.distributed.ExtendedBlockMatrix
import org.apache.spark.sql.types._
/**
  * Created by roir on 09/11/2016.
  */
class TestMatrixIO extends SparkTest(6,"SparkCommon"){
  trait UsesEdgeDF{
    val sqlC = sql
    import sqlC.implicits._
    val path = getClass.getClassLoader.getResource("A1.txt").getPath

    val schema = StructType(Array(
      StructField("from_id", StringType, false),
      StructField("to_id", StringType, false)
    ))

    val edgeDF = sqlC.read.format("com.databricks.spark.csv").
      option("delimiter", "\t").
      option("header", "false").
      schema(schema).
      load(path).
      select($"from_id",$"to_id").cache()
  }

  "ExtendedBlockMatrix IO" should "save matrix successfully" in new UsesEdgeDF {
    import sqlC.implicits._

    val edgeMat = edgeDF.
      toBlockMatrix($"from_id", $"to_id", rowsPerBlock = 2, colsPerBlock = 2)

    edgeMat.save("///mnt/tempMatrix/")
  }

  it should "load matrix successfully" in new UsesEdgeDF {

    val matrix = ExtendedBlockMatrix.load(sc, "///mnt/tempMatrix/")

    println(matrix.toLocalMatrix())
  }

  it should "save matrix with gzip codec" in new UsesEdgeDF {
    import sqlC.implicits._

    val edgeMat = edgeDF.
      toBlockMatrix($"from_id", $"to_id", rowsPerBlock = 2, colsPerBlock = 2)

    edgeMat.save("///mnt/tempMatrix_gz/", Some(classOf[GzipCodec]))
  }

  it should "load matrix with gzip codec" in new UsesEdgeDF {

    val matrix = ExtendedBlockMatrix.load(sc, "///mnt/tempMatrix_gz/")

    println(matrix.toLocalMatrix())
  }

  it should "save matrix with empty blocks" in new UsesEdgeDF {
    import sqlC.implicits._

    val edgeMat = edgeDF.
      toBlockMatrix($"from_id", $"to_id", rowsPerBlock = 2, colsPerBlock = 2)

    val emptyBlocksMat =
      edgeMat.updateBlocks(edgeMat.blocks.mapValues(x=>Matrices.zeros(x.numRows,x.numCols).asInstanceOf[DenseMatrix].toSparse))

    emptyBlocksMat.save("///mnt/tempMatrix_emptyBlocks/")
  }

  it should "load matrix with empty blocks" in new UsesEdgeDF {

    val matrix = ExtendedBlockMatrix.load(sc, "///mnt/tempMatrix_emptyBlocks/")

    println(matrix.toLocalMatrix())
  }
}
