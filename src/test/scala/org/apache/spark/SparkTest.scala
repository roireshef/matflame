package org.apache.spark

import org.apache.spark.sql.SQLContext
import org.scalatest._

/**
 * Created by roir on 03/02/2016.
 */
abstract class SparkTest(cores: Int = 6, name: String = "example-spark") extends FlatSpec with BeforeAndAfter{
  private val master = s"local[$cores]"
  private val appName = name

  protected var sc: SparkContext = _
  protected var sql: SQLContext = _

  before {
    val conf = new SparkConf()
      .setMaster(master)
      .setAppName(appName)

    sc = SparkContext.getOrCreate(conf)
    sc.setCheckpointDir("///mnt/")
    sql = SQLContext.getOrCreate(sc)
  }

  after {
    /*if (sc != null) {
      sc.stop()
    }*/
  }
}
