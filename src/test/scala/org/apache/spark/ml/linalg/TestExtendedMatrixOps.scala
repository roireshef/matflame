package org.apache.spark.ml.linalg

import org.apache.spark.SparkTest
import org.apache.spark.sql.types._
import Implicits._
import breeze.linalg.max
import org.apache.spark.mllib.linalg.distributed.{CoordinateMatrix, ExtendedBlockMatrix, MatrixEntry}
import org.apache.spark.mllib.linalg.{DenseMatrix=>LibDM, Matrices=>LibMatrices, SparseMatrix=>LibSM}
import org.apache.spark.mllib.linalg.distributed.functions.Implicits._
import org.apache.spark.mllib.linalg.MatrixUtils._
/**
 * Created by roir on 31/10/2016.
 */
class TestExtendedMatrixOps extends SparkTest(6,"SparkCommon"){
  trait UsesEdgeDF{
    val sqlC = sql
    import sqlC.implicits._
    val path = "src\\test\\resources\\A1.txt"

    val schema = StructType(Array(
      StructField("from_id", StringType, false),
      StructField("to_id", StringType, false)
    ))

    val edgeDF = sql.read.format("com.databricks.spark.csv").
      option("delimiter", "\t").
      option("header", "false").
      schema(schema).
      load(path).
      select($"from_id",$"to_id").
      cache()

//    edgeDF.show()
  }

  "normalization by rows" should "sum to one" in new UsesEdgeDF {
    import sqlC.implicits._
    val edgeMat = edgeDF.
      toBlockMatrix($"from_id", $"to_id", rowsPerBlock = 2, colsPerBlock = 2)

    val rowNormalized = edgeMat.computeWithDimStats({(cell,rowStat, colStat)=> cell / rowStat.get}, rowStats = Some(edgeMat.rowSums))
    val rowBlockError = rowNormalized.rowSums.blocks.mapValues{ mat =>
      var flag = false
      mat.foreachActive{case (i,j,v) => if (v!=1d) flag=true}
      flag
    }.collect()

    println(rowNormalized.toLocalMatrix())

    assert(rowBlockError.map(_._2).forall(_==false))


  }

  "multiplication of non-square matrices" should "result in the right dimensionality" in new UsesEdgeDF {
    import sqlC.implicits._
    val edgeMat = edgeDF.
      toBlockMatrix($"from_id", $"to_id", rowsPerBlock = 2, colsPerBlock = 2)

    edgeMat.slice(0L to 3L, 0L to 5L, true)
    val gramian = edgeMat.computeGramianMatrix(true)

    assert(gramian.numRows() == edgeMat.numCols())
    assert(gramian.numCols() == edgeMat.numCols())
  }

  "normalization by cols" should "sum to one" in new UsesEdgeDF {
    import sqlC.implicits._
    val edgeMat = edgeDF.
      toBlockMatrix($"from_id", $"to_id", rowsPerBlock = 2, colsPerBlock = 2)

    val colNormalized = edgeMat.computeWithDimStats({(cell,rowStat, colStat)=> cell / colStat.get},colStats = Some(edgeMat.colSums))
    val colBlockError = colNormalized.colSums.blocks.mapValues{ mat =>
      var flag = false
      mat.foreachActive{case (i,j,v) => if (v!=1d) flag=true}
      flag
    }.collect()

    println(colNormalized.toLocalMatrix())

    assert(colBlockError.map(_._2).forall(_==false))
  }

  "slicing" should "work on sequences" in new UsesEdgeDF {
    import sqlC.implicits._
    val edgeMat = edgeDF.
      toBlockMatrix($"from_id", $"to_id", rowsPerBlock = 2, colsPerBlock = 3)

    val slicedMat = edgeMat.slice(Seq[Long](0, 1, 3, 2), Seq[Long](0, 2, 4, 1), true)
    val localSlicedMatrix = slicedMat.toLocalMatrix()
    println(localSlicedMatrix)

    assert(localSlicedMatrix.getRowNames().get.toSeq.equals(Seq("A", "B", "D", "C")))
    assert(localSlicedMatrix.getColNames().get.toSeq.equals(Seq("A", "C", "E", "B")))
    assert(localSlicedMatrix.values.toSeq.equals(
      Seq(
        0.0, 1.0, 0.0, 1.0,
        1.0, 1.0, 1.0, 0.0,
        0.0, 1.0, 0.0, 0.0,
        1.0, 0.0, 0.0, 1.0)
    ))
  }

  it should "work on ranges" in new UsesEdgeDF {
    import sqlC.implicits._
    val edgeMat = edgeDF.
      toBlockMatrix($"from_id", $"to_id", rowsPerBlock = 2, colsPerBlock = 3)

    val slicedMat = edgeMat.slice(Range.Long(0, 6, 1), Range.Long(0, 6, 2), true)
    val localSlicedMatrix = slicedMat.toLocalMatrix()
    println(localSlicedMatrix)

    assert(localSlicedMatrix.getRowNames().get.toSeq.equals(Seq("A", "B", "C", "D", "E", "F")))
    assert(localSlicedMatrix.getColNames().get.toSeq.equals(Seq("A", "C", "E")))
    assert(localSlicedMatrix.values.toSeq.equals(
      Seq(
        0.0, 1.0, 1.0, 0.0, 0.0, 0.0,
        1.0, 1.0, 0.0, 1.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.0, 0.0, 1.0
      )
    ))
  }

  it should "work on other matrix's labels" in new UsesEdgeDF {
    import sqlC.implicits._
    val edgeMat = edgeDF.
      toBlockMatrix($"from_id", $"to_id", rowsPerBlock = 2, colsPerBlock = 2)

    val newRowLabels = sc.parallelize(Seq[(Long,String)]((0, "F"), (1, "D"), (2, "not_existing"), (3, "B")))
    val newColLabels = sc.parallelize(Seq[(Long,String)]((0, "B"), (1, "E"), (2, "A")))

    val sliced = edgeMat.sliceByLabels(Some(newRowLabels), Some(newColLabels), sparse = true)

    val rightMapping = Seq(
      (0,0) -> edgeMat.get(5,1),
      (0,1) -> edgeMat.get(5,4),
      (0,2) -> edgeMat.get(5,0),
      (1,0) -> edgeMat.get(3,1),
      (1,1) -> edgeMat.get(3,4),
      (1,2) -> edgeMat.get(3,0),
      (2,0) -> 0d,
      (2,1) -> 0d,
      (2,2) -> 0d,
      (3,0) -> edgeMat.get(1,1),
      (3,1) -> edgeMat.get(1,4),
      (3,2) -> edgeMat.get(1,0)
    )

    assert(sliced.getRowNames().get.collect().sortBy(_._1).map(_._2) sameElements Array("F", "D", null, "B"))
    assert(sliced.getColNames().get.collect().sortBy(_._1).map(_._2) sameElements Array("B", "E", "A"))
    rightMapping.foreach{case ((i,j), v) => assert(sliced.get(i,j) == v, s"sliced($i,$j) == ${sliced.get(i,j)} != $v")}
  }

  it should "work on other matrix's row labels" in new UsesEdgeDF {
    import sqlC.implicits._
    val edgeMat = edgeDF.
      toBlockMatrix($"from_id", $"to_id", rowsPerBlock = 2, colsPerBlock = 2)

    val newRowLabels = sc.parallelize(Seq[(Long,String)]((0, "F"), (1, "D"), (2, "not_existing"), (3, "B")))

    val sliced = edgeMat.sliceByLabels(Some(newRowLabels), None, sparse = true)

    val rightMapping = Seq(
      (0,0) -> edgeMat.get(5,1),
      (0,1) -> edgeMat.get(5,4),
      (0,2) -> edgeMat.get(5,0),
      (1,0) -> edgeMat.get(3,1),
      (1,1) -> edgeMat.get(3,4),
      (1,2) -> edgeMat.get(3,0),
      (2,0) -> 0d,
      (2,1) -> 0d,
      (2,2) -> 0d,
      (3,0) -> edgeMat.get(1,1),
      (3,1) -> edgeMat.get(1,4),
      (3,2) -> edgeMat.get(1,0)
    )

    assert(sliced.getRowNames().get.collect().sortBy(_._1).map(_._2) sameElements Array("F", "D", null, "B"))
    assert(
      sliced.getColNames().get.collect().sortBy(_._1).map(_._2) sameElements
      edgeMat.getColNames().get.collect().sortBy(_._1).map(_._2)
    )

    //array of values is of column major and thus transpose is necessary
    val expected = new LibDM(6,4,Array(
      0.0,1.0,0.0,0.0,1.0,0.0,
      0.0,0.0,1.0,0.0,0.0,0.0,
      0.0,0.0,0.0,0.0,0.0,0.0,
      1.0,0.0,1.0,0.0,1.0,1.0
    )).transpose

    assert((sliced.toLocalMatrix() :- expected).sum() == 0d)
  }

  "row-wise jaccard and col-wise jaccard on a symmetric matrix with non-square blocks" should "work" in new UsesEdgeDF {
    import sqlC.implicits._
    val edgeMat = edgeDF.
      toBlockMatrix($"from_id", $"to_id", rowsPerBlock = 3, colsPerBlock = 2)
    val colWiseJaccard = edgeMat.computeJaccardForCols()
    val rowWiseJaccard = edgeMat.computeJaccardForRows()

    println(colWiseJaccard.toLocalMatrix())
    colWiseJaccard.toLabeledCoordinates().collect.foreach(println)

    assert(1 == 1)
  }

  "row-wise jaccard and col-wise jaccard on a symmetric matrix (square-blocks)" should "be equal" in new UsesEdgeDF {
    import sqlC.implicits._
    val edgeMat = edgeDF.
      toBlockMatrix($"from_id", $"to_id", rowsPerBlock = 2, colsPerBlock = 2)
    val colWiseJaccard = edgeMat.computeJaccardForCols()
    val rowWiseJaccard = edgeMat.computeJaccardForRows()

    println(colWiseJaccard.toLocalMatrix())
    colWiseJaccard.toLabeledCoordinates().collect.foreach(println)

    assert((colWiseJaccard :- rowWiseJaccard).sum() == 0)
  }

  "matrix inner product" should "return valid output on max kernel" in {
    import org.apache.spark.mllib.linalg.MatrixUtils._

//    val xLocal = Matrices.dense(5,5,(0 until 5*5).toArray.map(_.toDouble)).transpose.asInstanceOf[DenseMatrix].toSparse
    val yLocal = LibMatrices.dense(5,5,Array(
      1, 0, 1, 0, 1,
      0, 1, 1, 0, 1,
      0, 1, 0, 0, 1,
      0, 1, 0, 1, 1,
      1, 0, 0, 0, 0
    )).asInstanceOf[LibDM].toSparse.transpose

    def distributeMatrix(m: LibSM) ={
      val rowsPerBlock = 2
      val colsPerBlock = 2
      val entriesLocal = for (row <- 0 until m.toDense.numRows;col<-0 until m.toDense.numCols) yield MatrixEntry(row,col,m(row,col))
      ExtendedBlockMatrix(
        new CoordinateMatrix(sc.parallelize(entriesLocal), m.numRows, m.numCols).
          toBlockMatrix(rowsPerBlock,colsPerBlock).blocks,
        rowsPerBlock,
        colsPerBlock,
        5L,
        5L,
        Some((1 to 5 map (x=>s"U$x")).toArray),
        Some(Array("a","b","c","d","e"))
      )
    }

//    val xEBM = parallelize(xLocal)
    val userTermMatrix = distributeMatrix(yLocal)

    /*
    1.0                 0.0                 0.3333333333333333  0.0                 0.2
    0.0                 1.0                 0.25                0.3333333333333333  0.75
    0.3333333333333333  0.25                1.0                 0.0                 0.5
    0.0                 0.3333333333333333  0.0                 1.0                 0.25
    0.2                 0.75                0.5                 0.25                1.0
    */
    val jaccardMatrix = userTermMatrix.computeJaccardForCols()

    val userJaccardTermsMatrix = userTermMatrix.innerProduct(
      jaccardMatrix,
      _.innerProductWithKernel(_ * _, max(_, _))(_),
      _.:::(_, max(_, _)),
      None
    )

    val expectedJaccardTerms = LibMatrices.dense(5,5,Array(
      1.0, 0.75, 1.0, 0.25, 1.0,
      1d/3, 1.0, 1.0, 1d/3, 1.0,
      0.2, 1.0, 0.5, 1d/3, 1.0,
      0.2, 1.0, 0.5, 1.0, 1.0,
      1.0, 0.0, 1d/3, 0.0, 0.2
    )).asInstanceOf[LibDM].transpose

    assert((expectedJaccardTerms :- userJaccardTermsMatrix.toLocalMatrix() :== 0d).sum() == 25,
          s"""
            |innerProduct (userJaccardTermsMatrix) result is invalid:
            |${userJaccardTermsMatrix.toLocalMatrix()}
            |expected:
            |$expectedJaccardTerms
          """.stripMargin)

    val expectedUserSimilarity = LibMatrices.dense(5,5,Array(
      1.0,  1.0,  1.0,  1.0,  1.0,
      1.0,  1.0,  1.0,  1.0,  1d/3,
      1.0,  1.0,  1.0,  1.0,  0.2,
      1.0,  1.0,  1.0,  1.0,  0.2,
      1.0,  1d/3, 0.2,  0.2,  1.0
    )).asInstanceOf[LibDM].transpose

    val userUserSimilarity = userTermMatrix.innerProduct(
      userJaccardTermsMatrix.transpose,
      _.innerProductWithKernel(_ * _, max(_, _))(_),
      _.:::(_, max(_, _)),
      None
    ).transpose

    assert((expectedUserSimilarity :- userUserSimilarity.toLocalMatrix() :== 0d).sum() == 25,
      s"""
         |innerProduct (userUserSimilarity) result is invalid:
         |${userUserSimilarity.toLocalMatrix()}
         |expected:
         |$expectedUserSimilarity
          """.stripMargin)
  }

  "setDiagonal" should "result with proper diagonal values" in new UsesEdgeDF{
    import sqlC.implicits._

    val edgeMat = edgeDF.
      toBlockMatrix($"from_id", $"to_id", rowsPerBlock = 3, colsPerBlock = 2)

    val result = edgeMat.setDiagonal(2d)

    val diagCoordinates = result.toLabeledCoordinates().filter{case (i,j,_) => i == j}

    assert(diagCoordinates.count() == edgeMat.numRows(), "(hot-)diagonal length should be equal to number of rows")

    assert(diagCoordinates.map { case (_, _, v) => v == 2d }.reduce(_ & _),
      s"all diagonal values should have been equal to 2.0")

}
}
