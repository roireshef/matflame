package org.apache.spark.sql

/**
  * Created by roir on 20/12/2016.
  */
object SQLCacheUtils {
  def isDFPersisted(df: DataFrame) = df.sparkSession.sharedState.cacheManager.lookupCachedData(df).isDefined
}
