package org.apache.spark.mllib.linalg

/**
 * Created by roir on 13/09/2016.
 */
trait LabeledMatrix[@specialized(Int, Long) T, +Base, RowLabel, ColLabel]{
  def numRows(): T
  def numCols(): T

  private var _rowNames: Option[Array[RowLabel]] = None
  private var _colNames: Option[Array[ColLabel]] = None
  
  def setRowNames(names: Array[RowLabel]): Base = {
    require(names.length==numRows(),
      s"setRowNames expects an array of size ${numRows()} but input array is of size ${names.length}")
    _rowNames = Some(names)
    this.asInstanceOf[Base]
  }
  def setRowNames(namesOpt: Option[Array[RowLabel]]): Base = {
    if (namesOpt.isDefined) setRowNames(namesOpt.get)
    this.asInstanceOf[Base]
  }
  def getRowNames() = _rowNames
  def hasRowNames() = _rowNames.isDefined

  def setColNames(names: Array[ColLabel]): Base = {
    require(names.length==numCols(),
      s"setColNames expects an array of size ${numCols()} but input array is of size ${names.length}")
    _colNames = Some(names)
    this.asInstanceOf[Base]
  }
  def setColNames(namesOpt: Option[Array[ColLabel]]): Base = {
    if (namesOpt.isDefined) setColNames(namesOpt.get)
    this.asInstanceOf[Base]
  }
  def getColNames() = _colNames
  def hasColNames() = _colNames.isDefined
}