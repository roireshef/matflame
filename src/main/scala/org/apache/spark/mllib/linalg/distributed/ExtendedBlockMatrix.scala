package org.apache.spark.mllib.linalg.distributed

import breeze.linalg.{Matrix => BM, DenseMatrix => BDM}
import org.apache.hadoop.io.compress.CompressionCodec
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.mllib.linalg.MatrixUtils._
import org.apache.spark.mllib.linalg._
import org.apache.spark.mllib.linalg.distributed.functions.{CountNonZeros, Sum}
import org.apache.spark.mllib.linalg.distributed.{ExtendedBlockMatrix => EBM}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.types._
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkContext, SparkException}

import scala.collection.immutable.Range
import scala.reflect.ClassTag
import scala.util.Try
/**
  * Created by roir on 11/09/2016.
  */
class ExtendedBlockMatrix(
                           blocksRDD: RDD[((Int,Int), Matrix)],
                           rowsPerBlock: Int,
                           colsPerBlock: Int,
                           nRows: Long,
                           nCols: Long,
                           rowNames: Option[RDD[(Long,String)]] = None, //never reference directly
                           colNames: Option[RDD[(Long,String)]] = None) //never reference directly
  extends BlockMatrix({
    val numRowBlocks = math.ceil(1.0 * nRows / rowsPerBlock).toInt
    val numColBlocks = math.ceil(1.0 * nCols / colsPerBlock).toInt

    val partitionedBlocks = blocksRDD.partitioner match{
      case Some(_:GridPartitioner) => blocksRDD
      case _ => blocksRDD.partitionBy(EBM.createPartitioner(numRowBlocks, numColBlocks, blocksRDD.partitions.length))
    }

    partitionedBlocks
    /*if (partitionedBlocks.count() == numRowBlocks * numColBlocks)
      partitionedBlocks
    else
      EBM.fillMissingBlocks(partitionedBlocks, nRows, nCols, rowsPerBlock, colsPerBlock)*/
  },rowsPerBlock, colsPerBlock, nRows, nCols)
    with LabeledBlockMatrixOps[ExtendedBlockMatrix]{

  //INTERNAL DEFINITIONS //
  override protected def instantiate(blocks: RDD[((Int, Int), Matrix)], rowsPerBlock: Int, colsPerBlock: Int, nRows: Long, nCols: Long): EBM =
  new ExtendedBlockMatrix(
    blocks,
    rowsPerBlock,
    colsPerBlock,
    nRows,
    nCols,
    None,
    None
  )
  override protected def update(blocks: RDD[((Int, Int), Matrix)], rowsPerBlock: Int, colsPerBlock: Int, nRows: Long, nCols: Long) =
    instantiate(
      blocks,
      rowsPerBlock,
      colsPerBlock,
      nRows,
      nCols
    ).setRowNames(getRowNames()).
      setColNames(getColNames())

  override def createPartitioner() = blocks.partitioner match{
    case Some(grid: GridPartitioner) => grid
    case _ => EBM.createPartitioner(numRowBlocks, numColBlocks, blocks.partitions.length)
  }

  // CONSTRUCTOR //
  if (rowNames.isDefined) setRowNames(rowNames.get)
  if (colNames.isDefined) setColNames(colNames.get)

  // PERSISTANCE UTILITY FUNCTIONS //

  override def setName(name: String) = {
    super.setName(name)
    setLabelsRDDsName(name)
    this
  }
  def getName() = blocks.name
  override def persist(storageLevel: StorageLevel): this.type = {
    super.persist(storageLevel)
    super.persistLabels(storageLevel)
    blocksKeys.persist(storageLevel)
    this
  }
  def unpersist(blocking: Boolean = true) = {
    blocks.unpersist(blocking)
    super.unpersistLabels(blocking)
    blocksKeys.unpersist(blocking)
    this
  }
  def materialize() = {
//    blocks.count()
    blocksKeys.count()
    getRowNames().foreach(_.count())
    getColNames().foreach(_.count())
    this
  }

  // LABEL UTILITY FUNCTIONS //

  override def ::: (other: BlockMatrix, f:(BM[Double], BM[Double])=>BM[Double], opName: String = "unknown") = {
    super.:::(other, f, opName).
      setRowNames(mergeRowLabelsWith(other)).
      setColNames(mergeColLabelsWith(other))
  }
  override def :: (other: Double, f:(BM[Double], Double)=>BM[Double], opName: String = "unknown") = {
    super.::(other, f, opName).
      setRowNames(this.getRowNames()).
      setColNames(this.getColNames())
  }
  override def toLocalMatrix(): LabeledDenseMatrix[String, String] = super.toLocalMatrix() match{
    case dMat: DenseMatrix =>
      new LabeledDenseMatrix(dMat).
        setRowNames(this.getRowNames().flatMap(rdd=>Some(rdd.sortByKey().values.collect()))).
        setColNames(this.getColNames().flatMap(rdd=>Some(rdd.sortByKey().values.collect())))
    case _ => throw new NotImplementedError("BlockMatrix.toLocalMatrix was used to output a DenseMatrix but something has changed(?)")
  }

  // MATRIX METHOD DEFINITIONS //

  override def transpose: ExtendedBlockMatrix = {
    val t = super.transpose
    new ExtendedBlockMatrix(
      t.blocks,
      t.rowsPerBlock,
      t.colsPerBlock,
      t.numRows(),
      t.numCols(),
      getColNames,
      getRowNames
    )
  }

  def compact(numPartitions: Int = this.blocks.partitions.length) = {
    val hotRowIndices = this.aggregateDimension(new CountNonZeros, 0).blocks.
      mapPartitions(_.flatMap{ case ((iBlock, jBlock), mat) =>
        def globalIdx(inner: Int) = iBlock * this.rowsPerBlock + inner
        mat.toCoordinates().flatMap {
          case LightMatrixEntry(i, j, v) if v != 0 => Some(globalIdx(i).toLong)
          case _ => None
        }.distinct
      }).sortBy(identity[Long])
    val hotColIndices = this.aggregateDimension(new CountNonZeros, 1).blocks.
      mapPartitions(_.flatMap{ case ((iBlock, jBlock), mat) =>
        def globalIdx(inner: Int) = jBlock * this.colsPerBlock + inner
        mat.toCoordinates().flatMap{
          case LightMatrixEntry(i,j,v) if v!=0 => Some(globalIdx(j).toLong)
          case _ => None
        }.distinct
      }).sortBy(identity[Long])

    this.slice(hotRowIndices, hotColIndices, true, numPartitions)
  }

  //to-indices that do not exist in either rowsFromTo or colsFromTo will result in null-string labels.
  override protected def sliceAndShip(rowsFromTo: RDD[(Long, Long)],
                                      colsFromTo: RDD[(Long, Long)],
                                      dstNumRows: Long,
                                      dstNumCols: Long,
                                      sparse: Boolean,
                                      dstNumPartitions: Int = blocks.partitions.length) = {
    def shipLabels(labelsRDD: RDD[(Long, String)], shipRDD: RDD[(Long, Long)]) =
      labelsRDD.join(shipRDD).map{case (fromIdx, (label, toIdx)) => (toIdx, label)}

    def fillMissingKeys(labelsRDD: RDD[(Long, String)], requiredNum: Long) =
      sc.parallelize(0L until requiredNum).map((_,null)).leftOuterJoin(labelsRDD).mapValues(_._2.orNull)

    ExtendedBlockMatrix(
      super.sliceAndShip(rowsFromTo, colsFromTo, dstNumRows, dstNumCols, sparse, dstNumPartitions),
      getRowNames().map(rowLabels=>fillMissingKeys(shipLabels(rowLabels,rowsFromTo),dstNumRows)),
      getColNames().map(colLabels=>fillMissingKeys(shipLabels(colLabels,colsFromTo),dstNumCols))
    )
  }

  @Deprecated
  def collectRange(rows: Range, cols: Range): LabeledDenseMatrix[String, String] = {
    assert(rows.step==1 && cols.step==1, "Currently slicing supports step size 1 only")
    val rowBlocks = (rows.start / rowsPerBlock) to (rows.end / rowsPerBlock)
    val colBlocks = (cols.start / colsPerBlock) to (cols.end / colsPerBlock)

    val values = new Array[Double](rows.length * cols.length)

    val localBlocks = blocks.filter{
      case ((i,j),mat) => rowBlocks.contains(i) && colBlocks.contains(j)
    }.collect()

    localBlocks.foreach{ case ((iBlock, jBlock), mat) =>
      val iOffset = iBlock * rowsPerBlock
      val jOffset = jBlock * colsPerBlock
      mat.foreachActive {
        case (i, j, v) if rows.contains(iOffset + i) && cols.contains(jOffset + j) =>
          values((j + jOffset  - cols.start) * rows.length + iOffset + i  - rows.start) = v
        case _ =>
      }
    }

    val localSlice = new LabeledDenseMatrix[String,String](rows.length, cols.length, values, false)
    if (getRowNames().isDefined) localSlice.setRowNames(EBM.sliceByKey(getRowNames().get, rows).collect())
    if (getColNames().isDefined) localSlice.setColNames(EBM.sliceByKey(getColNames().get, cols).collect())
    localSlice
  }

  override def multiply(other: BlockMatrix): ExtendedBlockMatrix = {
    val bMat = super.multiply(other)
    val colNames = other match {
      case dlm: DistributedLabeledMatrix[_] => dlm.getColNames()
      case _ => None
    }

    new ExtendedBlockMatrix(
      bMat.blocks.setName(this.blocks.name + "*" + other.blocks.name),
      bMat.rowsPerBlock,
      bMat.rowsPerBlock,
      this.nRows,
      other.numCols(),
      this.getRowNames(),
      colNames
    )
  }

  override def multiplySparse(other: BlockMatrix, suggestedNumPartitions: Option[Int] = None): ExtendedBlockMatrix = {
    val optColNames = other match {
      case dlm: DistributedLabeledMatrix[_] => dlm.getColNames()
      case _ => None
    }

    super.multiplySparse(other, suggestedNumPartitions).
      setRowNames(this.getRowNames()).
      setColNames(optColNames)
  }

  override def innerProduct(other: BlockMatrix,
                            kernel: (SparseMatrix,SparseMatrix)=>SparseMatrix,
                            reducer: (SparseMatrix,SparseMatrix)=>SparseMatrix,
                            suggestedNumPartitions: Option[Int]) = {
    val optColNames = other match {
      case dlm: DistributedLabeledMatrix[_] => dlm.getColNames()
      case _ => None
    }

    super.innerProduct(other, kernel, reducer, suggestedNumPartitions).
      setRowNames(this.getRowNames()).
      setColNames(optColNames)
  }


  //todo: unify with BlockMatrix multiplication
  def multiply(vector: BlockVector): BlockVector = {
    val result = EBM.multiplyWithBlockVec(this, vector)
    result.blocks.setName(this.blocks.name + "*" + vector.blocks.name)
    result
  }

  def expand[T <: MatrixFixture](bcMatrix: Broadcast[Matrix], fixtureBuilder: (Broadcast[Matrix]) => T)
                                (f: (T) => (Int, Int) => (Int, Int, Double) => Matrix) = {

    val newBlocks = blocks.mapPartitions{ iter =>
      val fixture = fixtureBuilder(bcMatrix)
      iter.map { case ((iBlock, jBlock), base) =>
        ((iBlock, jBlock), base expand f(fixture)(iBlock, jBlock))
      }
    }

    def createDefaultLabels(name: String, size: Long) = sc.range(0,size).map{ id => (id, name + id.toString) }

    val newRowLabels = bcMatrix.value match {
      case lm: LabeledMatrix[_, _, String, String] if lm.hasRowNames() && this.hasRowNames() =>
        val thisRowLabels = getRowNames().
          getOrElse(createDefaultLabels(getName(),numRows())).
          repartition(math.sqrt(sc.defaultParallelism).toInt)
        val bcMatRowLabels = sc.parallelize(
          lm.getRowNames().get.zipWithIndex.map(x=>(x._2.toLong, x._1)),
          math.sqrt(sc.defaultParallelism).toInt
        )
        Some(
          thisRowLabels.cartesian(bcMatRowLabels).
            map{case ((id1, label1),(id2, label2)) => ((id1, id2), (label1, label2))}.
            sortBy(_._1).values.map(Function.tupled(_ + "-" + _)).zipWithIndex().map(_.swap)
        )
      case _ => None
    }
    val newColLabels = bcMatrix.value match {
      case lm: LabeledMatrix[_, _, String, String] if lm.hasColNames() && this.hasColNames() =>
        val thisColLabels = getColNames().
          getOrElse(createDefaultLabels(getName(),numCols())).
          repartition(math.sqrt(sc.defaultParallelism).toInt)
        val bcMatColLabels = sc.parallelize(
          lm.getColNames().get.zipWithIndex.map(x=>(x._2.toLong, x._1)),
          math.sqrt(sc.defaultParallelism).toInt
        )
        Some(
          thisColLabels.cartesian(bcMatColLabels).
            map{case ((id1, label1),(id2, label2)) => ((id1, id2), (label1, label2))}.
            sortBy(_._1).values.map(Function.tupled(_ + "-" + _)).zipWithIndex().map(_.swap)
        )
      case _ => None
    }

    new ExtendedBlockMatrix(
      newBlocks,
      this.rowsPerBlock * bcMatrix.value.numRows,
      this.colsPerBlock * bcMatrix.value.numCols,
      this.numRows * bcMatrix.value.numRows,
      this.numCols * bcMatrix.value.numCols,
      newRowLabels,
      newColLabels
    )
  }

  /*def fillBlockIndices(numRowBlocks: Int, numColBlocks: Int, partitions: Int = blocks.partitions.length) = {
    val sc = blocks.sparkContext

    val rowBlocksIndices = sc.parallelize(0 until numRowBlocks, math.sqrt(sc.defaultParallelism).toInt)
    val colBlocksIndices = sc.parallelize(0 until numColBlocks, math.sqrt(sc.defaultParallelism).toInt)

    val fullBlockIndices = rowBlocksIndices.cartesian(colBlocksIndices).map((_,null))

    val bcMatrixGenerator = sc.broadcast(new EBM.EmptyBlockGenerator(this))

    val filledBlocks =
      fullBlockIndices.
        leftOuterJoin(this.blocks, GridPartitioner(numRowBlocks, numColBlocks, partitions)).
        mapPartitions { iter =>
          val generator = bcMatrixGenerator.value
          iter.map {
            case ((iBlock, jBlock), (_, optMat)) =>
              ((iBlock, jBlock), optMat.getOrElse(generator.generate(iBlock, jBlock)))
          }
        }

    new ExtendedBlockMatrix(
      filledBlocks,
      rowsPerBlock,
      colsPerBlock,
      numRows(),
      numCols(),
      getRowNames(),
      getColNames()
    )
  }*/

  // WRITE TO DISK //

  def save(path: String, codec: Option[Class[_ <: CompressionCodec]] = None) = EBM.save(this, path, codec)

  // TEMP //

  def toLabeledCoordinates() = {
    def replaceByLabelOpt[T](rdd: RDD[(Long, (T, Double))], labels: Option[RDD[(Long, String)]]) =
      if (labels.isDefined)
        rdd.join(labels.get).map{ case (dim1value,((dim2value,v),dim1Label)) => (dim1Label, (dim2value, v))}
      else
        rdd.map{case (dim1value,(dim2value,v)) => (dim1value.toString,(dim2value,v))}

    val coordsByRow = super.toCoordinateMatrix().entries.map{case MatrixEntry(i,j,v) => (i,(j,v))}
    val coordsByRowLabel = replaceByLabelOpt(coordsByRow, getRowNames)
    val coordsByCol = coordsByRowLabel.map{case (i,(j,v)) => (j,(i,v))}
    val coordsByColLabel = replaceByLabelOpt(coordsByCol, getColNames)

    coordsByColLabel.map{ case (jLabel, (iLabel, v)) => (iLabel, jLabel, v)}
  }

}

trait MatrixFixture extends Serializable

object ExtendedBlockMatrix{
  def apply(blocks: RDD[((Int,Int), Matrix)], rowsPerBlock: Int, colsPerBlock: Int, nRows: Long, nCols: Long,
            rowNames: Option[Array[String]], colNames: Option[Array[String]]): EBM = {
    val newMatrix = new ExtendedBlockMatrix(
      blocks,
      rowsPerBlock,
      colsPerBlock,
      nRows,
      nCols
    )

    rowNames.foreach(newMatrix.setRowNames)
    colNames.foreach(newMatrix.setColNames)

    newMatrix
  }
  def apply(bMat: BlockMatrix, rowNames: Option[RDD[(Long,String)]] = None, colNames: Option[RDD[(Long,String)]] = None) =
    new EBM(bMat.blocks, bMat.rowsPerBlock, bMat.colsPerBlock, bMat.numRows(), bMat.numCols(), rowNames, colNames)

  private def createPartitioner(rows: Int, cols: Int, rowsPerPart: Int, colsPerPart: Int): GridPartitioner = {
    GridPartitioner(rows, cols, rowsPerPart, colsPerPart)
  }

  private def createPartitioner(rows: Int, cols: Int, suggestedNumPartitions: Int): GridPartitioner = {
    GridPartitioner(rows, cols, suggestedNumPartitions)
  }

  def save(matrix: EBM, path: String, codec: Option[Class[_ <: CompressionCodec]] = None) = {
    val sql = SQLContext.getOrCreate(matrix.blocks.sparkContext)
    import sql.implicits._

    val stippedRoot = path.stripSuffix("/")

    matrix.blocks.
      map{case ((i,j),mat)=>(i,j,mat)}.
      toDF().toDF("i","j","mat").
      select($"i", $"j", MatrixSerDe.serialize($"mat").as("mat_data")).
      select($"i", $"j", $"mat_data.tpe", $"mat_data.numRows", $"mat_data.numCols", $"mat_data.colPtrs",
        $"mat_data.rowIndices", $"mat_data.values", $"mat_data.isTransposed").
      write.format("com.databricks.spark.csv").
      option("delimiter", "\t").
      option("codec",codec.map(_.getName).orNull).
      option("header", "false").
      save(s"$stippedRoot/blocks")

    matrix.blocks.sparkContext.parallelize(Seq(
      "numRows" -> matrix.numRows(),
      "numCols" -> matrix.numCols(),
      "rowsPerBlock" -> matrix.rowsPerBlock.toLong,
      "colsPerBlock" -> matrix.colsPerBlock.toLong
    )).toDF().repartition(1).write.
      format("com.databricks.spark.csv").
      option("delimiter", "\t").
      option("codec",codec.map(_.getName).orNull).
      option("header", "false").
      save(s"$stippedRoot/meta")

    matrix.getRowNames().foreach(_.toDF().
      write.format("com.databricks.spark.csv").
      option("delimiter", "\t").
      option("codec",codec.map(_.getName).orNull).
      option("header", "false").
      save(s"$stippedRoot/rowNames")
    )

    matrix.getColNames().foreach(_.toDF().
      write.format("com.databricks.spark.csv").
      option("delimiter", "\t").
      option("codec",codec.map(_.getName).orNull).
      option("header", "false").
      save(s"$stippedRoot/colNames")
    )
  }

  def load(sc: SparkContext, path: String) = {
    val sql = SQLContext.getOrCreate(sc)
    import sql.implicits._

    val stippedRoot = path.stripSuffix("/")

    val blockSchema = StructType(Array(
      StructField("i", IntegerType, false),
      StructField("j", IntegerType, false),
      StructField("tpe", ByteType, nullable = false),
      StructField("numRows", IntegerType, nullable = false),
      StructField("numCols", IntegerType, nullable = false),
      StructField("colPtrs", StringType, nullable = true),
      StructField("rowIndices", StringType, nullable = true),
      StructField("values", StringType, nullable = true),
      StructField("isTransposed", BooleanType, nullable = false)
    ))

    val labelsSchema = StructType(Array(
      StructField("id", LongType, false),
      StructField("label", StringType, false)
    ))

    val metaSchema = StructType(Array(
      StructField("conf", StringType, false),
      StructField("value", LongType, false)
    ))

    val blockMatRDD = sql.read.format("com.databricks.spark.csv").
      option("delimiter", "\t").
      option("header", "false").
      schema(blockSchema).
      load(s"$stippedRoot/blocks").
      select($"i", $"j", MatrixSerDe.deserialize($"tpe", $"numRows", $"numCols", $"colPtrs",
        $"rowIndices", $"values", $"isTransposed")).
      rdd.map{row => ((row.getAs[Int](0), row.getAs[Int](1)), row.getAs[Matrix](2))}

    val conf = sql.read.format("com.databricks.spark.csv").
      option("delimiter", "\t").
      option("header", "false").
      schema(metaSchema).
      load(s"$stippedRoot/meta").
      collect().
      map{row => row.getAs[String](0) -> row.getAs[Long](1)}.
      toMap

    val rowNames = Try(
      sql.read.format("com.databricks.spark.csv").
        option("delimiter", "\t").
        option("header", "false").
        schema(labelsSchema).
        load(s"$stippedRoot/rowNames").
        repartition(sc.defaultParallelism).
        rdd.map{row => (row.getAs[Long](0), row.getAs[String](1))}
    ).toOption

    val colNames = Try(
      sql.read.format("com.databricks.spark.csv").
        option("delimiter", "\t").
        option("header", "false").
        schema(labelsSchema).
        load(s"$stippedRoot/colNames").
        repartition(sc.defaultParallelism).
        rdd.map{row => (row.getAs[Long](0), row.getAs[String](1))}
    ).toOption

    new ExtendedBlockMatrix(
      blockMatRDD,
      conf("rowsPerBlock").toInt,
      conf("colsPerBlock").toInt,
      conf("numRows"),
      conf("numCols"),
      rowNames,
      colNames
    )
  }

  //RDD UTILITY FUNCTIONS
  @Deprecated
  private def sliceByKey[T: ClassTag](rdd:RDD[(Long, T)], keyRange: Range): RDD[T] =
  rdd.filter(pair=>keyRange.contains(pair._1)).sortByKey().values

  //OP UTILITY FUNCTIONS
  //TODO: modify this to use simulateVecBroadcastDistributed on transposed vector
  private def multiplyWithBlockVec(it: ExtendedBlockMatrix, vec: BlockVector): BlockVector = {
    require(it.numCols() == vec.numRows(), "The number of columns of A and the number of rows " +
      s"of B must be equal. A.numCols: ${it.numCols()}, B.numRows: ${vec.numRows()}.")

    //create partitioner for BlockVector blocks. Number of blocks in the BlockVector result will be
    //the same as it.numRowBlocks. use one partition per block
    val resultPartitioner = EBM.createPartitioner(it.numRowBlocks, 1, 1, 1)

    if (it.colsPerBlock == vec.rowsPerBlock) {
      //First, bring all block of the vector to the blocks of the matrix
      val thisPartitioner = it.blocks.partitioner.getOrElse(it.createPartitioner())
      val thisMatrix = it.blocks.keys.collect()
      val thisDestinations = thisMatrix.map { case (rowIndex, colIndex) =>
        ((rowIndex, colIndex), thisPartitioner.getPartition((rowIndex, colIndex)))
      }.toMap
      val vecDestinations = vec.blocks.keys.collect().map { case (rowIndex, colIndex) =>
        val leftCounterparts = thisMatrix.filter(_._2 == rowIndex)
        val partitions = leftCounterparts.map(b => thisDestinations(b))
        ((rowIndex, colIndex), partitions.toSet)
      }.toMap

      // Prepare blocks of matrix for multiplication (they stay where they are)
      val flatThis = it.blocks.mapPartitions ( iter => iter.map{ case ((blockRowIndex, blockColIndex), block) =>
        (thisDestinations((blockRowIndex, blockColIndex)), (blockRowIndex, blockColIndex, block))
      }, preservesPartitioning = true)
      // Each block of B must be multiplied with the corresponding blocks in each row of A.
      val flatVec = vec.blocks.flatMap { case ((blockRowIndex, blockColIndex), block) =>
        val destinations = vecDestinations.getOrElse((blockRowIndex, blockColIndex), Set.empty)
        destinations.map(j => (j, (blockRowIndex, blockColIndex, block)))
      }
      val newBlocks = flatThis.cogroup(flatVec, thisPartitioner).flatMap { case (pId, (a, b)) =>
        a.flatMap { case (leftRowIndex, leftColIndex, leftBlock) =>
          b.filter(_._1 == leftColIndex).map { case (rightRowIndex, rightColIndex, rightBlock) =>
            val C = rightBlock match {
              case dense: DenseMatrix => leftBlock.multiply(dense)
              case sparse: SparseMatrix => leftBlock.multiply(sparse.toDense)
              case _ =>
                throw new SparkException(s"Unrecognized matrix type ${rightBlock.getClass}.")
            }
            ((leftRowIndex, 0), C)
          }
        }
//        .reduceByKey(vec.blocks.partitioner.getOrElse(vec.createPartitioner()), (a, b) => a + b).mapValues(Matrices.fromBreeze)
      }.aggregateByKey(null.asInstanceOf[BDM[Double]], resultPartitioner)(
        {(accum, mat) => if (accum == null) mat.asBreeze.asInstanceOf[BDM[Double]] else accum + mat.asBreeze.asInstanceOf[BDM[Double]]},
        {(mat1, mat2) => mat1 + mat2}
      ).mapValues(Matrices.fromBreeze)
      new BlockVector(newBlocks, it.rowsPerBlock, it.numRows())
    } else throw new SparkException("colsPerBlock of A doesn't match rowsPerBlock of B. " +
      s"A.colsPerBlock: ${it.colsPerBlock}, B.rowsPerBlock: ${vec.rowsPerBlock}")

  }
}