package org.apache.spark.mllib.linalg

import breeze.linalg.{CSCMatrix => BSM, DenseMatrix => BDM, Matrix => BM}

/**
 * Created by roir on 06/11/2016.
 */
sealed trait LocalMatrixOps[Class <: Matrix, BreezeClass <: BM[Double]]{
  def ::(scalar: Double, f:(BreezeClass, Double) => BM[Double]): Class

  def :::(otherMat:Class, f:(BreezeClass, BreezeClass) => BM[Double]): Class

  def :+(scalar: Double) = ::(scalar, _ :+ _)
  def :-(scalar: Double) = ::(scalar, _ :- _)
  def :*(scalar: Double) = ::(scalar, _ :* _)
  def :/(scalar: Double) = ::(scalar, _ :/ _)
  def :^(scalar: Double) = ::(scalar, _ :^ _)

  def :+(otherMat:Class): Class
  def :-(otherMat:Class): Class
  def :*(otherMat:Class): Class
  def :/(otherMat:Class): Class
  def :^(otherMat:Class): Class
}

trait LocalSparseMatrixOps extends LocalMatrixOps[SparseMatrix, BSM[Double]]{
  def :+(otherMat:SparseMatrix) = :::(otherMat, _ :+ _)
  def :-(otherMat:SparseMatrix) = :::(otherMat, _ :- _)
  def :*(otherMat:SparseMatrix) = :::(otherMat, _ :* _)
  def :/(otherMat:SparseMatrix) = :::(otherMat, _ :/ _)
  def :^(otherMat:SparseMatrix) = :::(otherMat, _ :^ _)
}

trait LocalDenseMatrixOps extends LocalMatrixOps[DenseMatrix, BDM[Double]]{
  def :+(otherMat:DenseMatrix) = :::(otherMat, _ :+ _)
  def :-(otherMat:DenseMatrix) = :::(otherMat, _ :- _)
  def :*(otherMat:DenseMatrix) = :::(otherMat, _ :* _)
  def :/(otherMat:DenseMatrix) = :::(otherMat, _ :/ _)
  def :^(otherMat:DenseMatrix) = :::(otherMat, _ :^ _)
}