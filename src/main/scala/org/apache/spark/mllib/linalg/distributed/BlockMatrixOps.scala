package org.apache.spark.mllib.linalg.distributed

import breeze.linalg.{CSCMatrix => BSM, DenseMatrix => BDM, Matrix => BM, max => BMax, min => BMin, sum => BSum}
import org.apache.spark.mllib.linalg.MatrixUtils._
import org.apache.spark.mllib.linalg._
import org.apache.spark.mllib.linalg.distributed.functions.AggregateFunction
import org.apache.spark.mllib.linalg.distributed.utils.RDDImplicits._
import org.apache.spark.rdd.RDD
import org.apache.spark.{HashPartitioner, SparkException}

import scala.reflect.ClassTag

/**
 * Created by roir on 13/09/2016.
  */

trait LabeledBlockMatrixOps[+Class<:BlockMatrix with OperationsOnSelf[_] with OperationsBase[Class] with DistributedLabeledMatrix[Class]]
  extends BlockMatrixOps[Class]
    with CanBeSlicedByLabel[Class]
    with DistributedLabeledMatrix[Class]

trait BlockMatrixOps[+Class<:BlockMatrix with OperationsOnSelf[_] with OperationsBase[Class]]
  extends BlockMatrix
    with BlockMatrixAssertions
    with OperationsBase[Class]
    with OperationsOnSelf[Class]
    with OperationsVsScalar[Class]
    with ElementWiseOperations[Class]
    with DimensionWiseOperations[Class]
    with CanBeSliced[Class]
    with CanMultiply[Class]

trait BlockVectorOps
  extends BlockMatrix
    with OperationsBase[BlockVector]
    with OperationsOnSelf[BlockVector]
    with OperationsVsScalar[BlockVector]
    with ElementWiseOperations[BlockVector]

sealed trait OperationsBase[+Class<:BlockMatrix with OperationsBase[Class]]
  extends BlockMatrix{

  type BlockDestinationRDD = RDD[((Int, Int), Set[Int])]

  val blocksKeys: RDD[(Int, Int)] = blocks.keys

  def setName(name: String): Class = {
    blocks.setName(name + "_Blocks")
    blocksKeys.setName(name + "_BlocksKeys")
    this.asInstanceOf[Class]
  }

  def get(i: Long, j: Long) = {
    val rowBlock = i / rowsPerBlock toInt
    val colBlock = j / colsPerBlock toInt
    val inBlockRow = i % rowsPerBlock toInt
    val inBlockCol = j % colsPerBlock toInt

    val arr = blocks.
      filter { case ((iBlock, jBlock), mat) => iBlock == rowBlock && jBlock == colBlock }.
      values.
      map(_.apply(inBlockRow, inBlockCol)).
      collect()

    if (arr.length>0) arr.head else 0d
  }

  protected def sc = this.blocks.sparkContext
  protected def instantiate(blocks: RDD[((Int,Int), Matrix)], rowsPerBlock: Int, colsPerBlock: Int, nRows: Long, nCols: Long): Class
  protected def update(blocks: RDD[((Int,Int), Matrix)], rowsPerBlock: Int, colsPerBlock: Int, nRows: Long, nCols: Long): Class
  protected def update(blocks: RDD[((Int,Int), Matrix)], nRows: Long, nCols: Long): Class = update(blocks, this.rowsPerBlock, this.colsPerBlock, nRows, nCols)
  protected[linalg] def updateBlocks(blocks: RDD[((Int,Int), Matrix)]): Class = update(blocks, this.rowsPerBlock, this.colsPerBlock, numRows(), numCols())
  protected def maxPartitioner(other: BlockMatrix) = {
    if (blocks.partitions.length >= other.blocks.partitions.size)
      createPartitioner()
    else
      other.createPartitioner()
  }

  def fillMissingBlocks(): Class = {
    val rowBlocks = math.ceil(1.0 * numRows / rowsPerBlock).toInt
    val colBlocks = math.ceil(1.0 * numCols / colsPerBlock).toInt

    val rowBlockIndices = sc.parallelize(0 until rowBlocks, math.min(rowBlocks, math.sqrt(sc.defaultParallelism)).toInt)
    val colBlockIndices = sc.parallelize(0 until colBlocks, math.min(colBlocks, math.sqrt(sc.defaultParallelism)).toInt)

    val fullBlocksRDD = (rowBlockIndices cartesian colBlockIndices map ((_,null))).
      leftOuterJoin(blocks, blocks.partitioner.get).mapPartitions({ iter =>
      val generator = new EmptyBlockGenerator(numRows, numCols, rowsPerBlock, colsPerBlock)
      iter.map {
        case ((iBlock, jBlock), (_, optMat)) =>
          ((iBlock, jBlock), optMat.getOrElse(generator.generate(iBlock, jBlock)))
      }
    }, true)

    updateBlocks(fullBlocksRDD)
  }
}

sealed trait BlockMatrixAssertions extends BlockMatrix{
  def assertEqualDimensions(other: BlockMatrix, opName: String) = {
    assertEqualRowBlocks(other, opName)
    assertEqualColBlocks(other, opName)
  }
  def assertEqualRowBlocks(other: BlockMatrix, opName: String) = {
    require(this.numRows() == other.numRows(),
      s"Operator $opName requires equal number of rows. " +
        s"left side has ${this.numRows()} rows while right side has ${other.numRows()}")
    require(this.rowsPerBlock == other.rowsPerBlock,
      s"Operator $opName requires equal number of rowsPerBlock. " +
        s"left side has rowsPerBlock=${this.rowsPerBlock} rows while right side "+
        s"has rowsPerBlock=${other.rowsPerBlock}")
  }
  def assertEqualColBlocks(other: BlockMatrix, opName: String) = {
    require(this.numCols() == other.numCols(),
      s"Operator $opName requires equal number of cols. " +
        s"left side has ${this.numCols()} cols while right side has ${other.numCols()}")
    require(this.colsPerBlock == other.colsPerBlock,
      s"Operator $opName requires equal number of rowsPerBlock. " +
        s"left side has rowsPerBlock=${this.colsPerBlock} rows while right side "+
        s"has rowsPerBlock=${other.colsPerBlock}")
  }
}

sealed trait OperationsOnSelf[+Class<:BlockMatrix with OperationsBase[Class]]
  extends BlockMatrix
    with BlockMatrixAssertions
    with OperationsBase[Class] {
  def sum() = blocks.values.aggregate(0d)({
    case (accumulator, mat:DenseMatrix) => accumulator + BSum(mat.asBreeze.toDenseMatrix)
    case (accumulator, mat:SparseMatrix) => accumulator + BSum(mat.asBreeze.asInstanceOf[BSM[Double]])
  }, _ + _)
  def min() = blocks.values.aggregate(Double.PositiveInfinity)({
    case (accumulator, mat:DenseMatrix) => math.max(accumulator, BMin(mat.asBreeze.toDenseMatrix))
    case (accumulator, mat:SparseMatrix) => math.max(accumulator, BMin(mat.asBreeze.asInstanceOf[BSM[Double]]))
  }, math.max)
  def max() = blocks.values.aggregate(Double.NegativeInfinity)({
    case (accumulator, mat:DenseMatrix) => math.max(accumulator, BMax(mat.asBreeze.toDenseMatrix))
    case (accumulator, mat:SparseMatrix) => math.max(accumulator, BMax(mat.asBreeze.asInstanceOf[BSM[Double]]))
  }, math.max)

  //TODO untested!
  def setDiagonal(v: Double) = {
    require(numRows() == numCols(),
      s"setDiagonal was called but numRows == ${numRows()} and numCols == ${numCols()}")

    val newBlocks = blocks.map{ case ((iBlock, jBlock), mat) =>
      def globalIdx(blockIdx: Int, innerIdx: Int, elemsPerBlock: Int) = blockIdx * elemsPerBlock + innerIdx toLong
      def innerIdx(blockIdx: Int, globalIdx: Long, elemsPerBlock: Int) =
        if (globalIdx / elemsPerBlock == blockIdx)
          globalIdx % elemsPerBlock toInt
        else
          throw new IllegalArgumentException(
            s"wrong globalIdx ($globalIdx) for block $blockIdx with elemsPerBlock = $elemsPerBlock")
      val rowGlobalRange = globalIdx(iBlock, 0, rowsPerBlock) to
        math.min(globalIdx(iBlock, rowsPerBlock-1, rowsPerBlock), numRows() - 1)
      val colGlobalRange = globalIdx(jBlock, 0, colsPerBlock) to
        math.min(globalIdx(jBlock, colsPerBlock-1, colsPerBlock), numCols() - 1)
      val diagonalGlobalIdxs = rowGlobalRange intersect colGlobalRange
      val innerDiagonalCoordinates = diagonalGlobalIdxs.map{idx =>
        val innerRow = innerIdx(iBlock, idx, rowsPerBlock)
        val innerCol = innerIdx(jBlock, idx, colsPerBlock)
        (innerRow, innerCol)
      }
      val newMat = mat match{
        case smat: SparseMatrix =>
          SparseMatrix.fromCOO(
            smat.numRows,
            smat.numCols,
            smat.toCoordinates().
              map(lme => (lme.i, lme.j, lme.value)).
              filterNot{case (i,j,_)=> innerDiagonalCoordinates.contains((i,j))} union
            innerDiagonalCoordinates.map{case (i,j) => (i,j,v)}
          )
        case dmat: DenseMatrix =>
          innerDiagonalCoordinates.foreach{ case (i,j) => dmat.update(i,j,1d)}
          dmat
      }
      ((iBlock, jBlock), newMat)
    }

    updateBlocks(newBlocks)
  }
}

sealed trait OperationsVsScalar[+Class<:BlockMatrix with OperationsOnSelf[_] with OperationsBase[Class]]
  extends BlockMatrix
    with BlockMatrixAssertions
    with OperationsBase[Class] {

  def ::(scalar: Double, f:(BM[Double], Double)=>BM[Double], opName: String = "unknown") = {
    updateBlocks(
      blocks.
        mapValues(mat => Matrices.fromBreeze(f(mat.asBreeze, scalar))).
        setName(this.blocks.name + "::["  + opName + "] scalar")
    )
  }
  def :* (scalar: Double) = ::(scalar,_ :* _, ":*")
  def :+ (scalar: Double) = ::(scalar,_ :+ _, ":+")
  def :- (scalar: Double) = ::(scalar,_ :- _, ":-")
  def :/ (scalar: Double) = ::(scalar,_ :/ _, ":/")
  def :^ (scalar: Double) = ::(scalar,_ :^ _, ":^")

  def norm(l: Int = 2) = math.pow(this :^ l sum(), 1/l)
}

sealed trait ElementWiseOperations[+Class<:BlockMatrix with OperationsBase[Class]]
  extends BlockMatrix
    with BlockMatrixAssertions
    with OperationsBase[Class]{

  def :::(other: BlockMatrix, f:(BM[Double], BM[Double])=>BM[Double], opName: String = "unknown") = {
    assertEqualDimensions(other, opName)
    updateBlocks(
      blocks.join(other.blocks, maxPartitioner(other)).mapValues{
        case (a, b) => Matrices.fromBreeze(f(a.asBreeze, b.asBreeze))
      }.setName(this.blocks.name + ":::["  + opName + "]" + other.blocks.name)
    )
  }
  def :* (other: BlockMatrix) = :::(other,_ :* _, ":*")
  def :+ (other: BlockMatrix) = :::(other,_ :+ _, ":+")
  def :- (other: BlockMatrix) = :::(other,_ :- _, ":-")
  def :/ (other: BlockMatrix) = :::(other,_ :/ _, ":/")
  def :^ (other: BlockMatrix) = :::(other,_ :^ _, ":^")
}

sealed trait DimensionWiseOperations[+Class<:BlockMatrix with OperationsBase[Class]]
  extends BlockMatrix
    with BlockMatrixAssertions
    with OperationsBase[Class]{

  def aggregateDimension[U: ClassTag](f: AggregateFunction[U], dim: Int): BlockVector =
    aggregateDimension(f.zeroValue, f.seqOp, f.combOp, f.finallyOp)(dim)

  def aggregateDimension[U: ClassTag](
    zeroValue: U,
    seqOp: (U, Double) => U,
    combOp: (U, U) => U,
    finallyOp: U => Double)(dim: Int): BlockVector = {

    def maskTuple2(tuple: (Int, Int), idx: Int) = idx match{
      case 0 => Tuple2(tuple._1, 0)
      case 1 => Tuple2(0, tuple._2)
      case o => throw new ArrayIndexOutOfBoundsException(s"cannot mask on dim $o")
    }

    val newBlocks = this.blocks.
      map{case ((iBlock, jBlock), mat) => (maskTuple2((iBlock, jBlock), dim), mat)}.
      aggregateByKey(Option.empty[Array[U]])({
        case (accumOpt, mat) =>
          val n = maskTuple2((mat.numRows, mat.numCols), dim).sumAs[Int]
          val accum = accumOpt.getOrElse(Array.fill(n)(zeroValue))
          mat.foreachActive { (i, j, v) =>
            val index = maskTuple2((i, j), dim).sumAs[Int]
            accum.update(index, seqOp(accum(index), v))
          }
          Some(accum)
      },{
        case (accumOpt1, accumOpt2) =>
          Some(Seq(accumOpt1, accumOpt2).flatten.reduce(_ zip _ map Function.tupled(combOp)))
      }).mapValues(_.get map finallyOp).mapValues(arr =>Matrices.dense(arr.length, 1, arr))

    if (dim==0)
      BlockVector.apply(new BlockMatrix(newBlocks,this.rowsPerBlock,1))
    else
      BlockVector.transposed(new BlockMatrix(newBlocks.mapValues(_.transpose),1,this.colsPerBlock))
  }

  /**
    * Executes an element-wise operation involves row-statistics and/or col-statistics
    *
    * @param f function to apply on the tuple (cell, row-stat-value, col-stat-value)
    * @param rowStats row stats vector
    * @param colStats col stats vector
    */
  def computeWithDimStats(  f:(Double, Option[Double], Option[Double]) => Double,
                            rowStats: Option[BlockVector] = None,
                            colStats: Option[BlockVector] = None) = {

    //Compute destinations for each BlockVector's block
    //Note: it calls .get but in a lazy fashion so it is guaranteed to run only on defined objects
    lazy val rowStatsDestinations = simulateVectorBroadcast(rowStats.get)
    lazy val colStatsDestinations = simulateVectorBroadcast(colStats.get)

    val flatThis = blocks.map { case ((blockRowIndex, blockColIndex), block) =>
      val partition = this.createPartitioner().getPartition((blockRowIndex, blockColIndex))
      (partition, ((blockRowIndex, blockColIndex), block))
    }

    val flatRowStats = rowStats.map(_.blocks.leftOuterJoin(rowStatsDestinations).flatMap{
      case ((blockRowIndex, _),(block, destinations)) =>
        destinations.getOrElse(Set.empty).map(destPartition => (destPartition, (blockRowIndex, block)))
    })

    val flatColStats = colStats.map(_.blocks.leftOuterJoin(colStatsDestinations).flatMap{
      case ((_, blockColIndex),(block, destinations)) =>
        destinations.getOrElse(Set.empty).map(destPartition => (destPartition, (blockColIndex, block)))
    })

    val newBlocks = flatThis.cogroupOpt(flatRowStats, flatColStats, this.createPartitioner()).flatMap {
      case (_, (thisBlocks, rowStatsBlocks, colStatsBlocks)) =>
        val rowStatsBlocksMap = rowStatsBlocks.map(_.toMap)
        val colStatsBlocksMap = colStatsBlocks.map(_.toMap)
        thisBlocks.map { case ((thisRowBlockIndex, thisColBlockIndex), thisMat) =>
          val rowStatsArray = rowStatsBlocksMap.map(_(thisRowBlockIndex).toArray)
          val colStatsArray = colStatsBlocksMap.map(_(thisColBlockIndex).toArray)
          val processed = thisMat.toCoordinates().map( entry =>
            (entry.i,entry.j,f(entry.value, rowStatsArray.map(_(entry.i)), colStatsArray.map(_(entry.j))))
          )
          ((thisRowBlockIndex, thisColBlockIndex), SparseMatrix.fromCOO(thisMat.numRows, thisMat.numCols, processed).asInstanceOf[Matrix])
        }
    }.partitionBy(this.createPartitioner())

    updateBlocks(newBlocks)
  }

  private def simulateVectorBroadcast[RowLabel: ClassTag, ColLabel: ClassTag](vec: BlockVector): BlockDestinationRDD = {
    if (vec.numCols()==1){  //vertical vector (regular)
      this.assertEqualRowBlocks(vec, "simluateVecBroadcast")
      this.blocksKeys.groupBy(_._1).map { case (thisRowIndex, thisRowColIndices) =>
        val partitions = thisRowColIndices.map(this.createPartitioner().getPartition)
        ((thisRowIndex, 0), partitions.toSet)
      }
    } else{                 //horizontal vector (transposed)
      this.assertEqualColBlocks(vec, "simluateVecBroadcast")
      this.blocksKeys.groupBy(_._2).map { case (thisColIndex, thisRowColIndices) =>
        val partitions = thisRowColIndices.map(this.createPartitioner().getPartition)
        ((0, thisColIndex), partitions.toSet)
      }
    }
  }

  private implicit class NumericTuple(tuple: Product){
    def sumAs[Elem: Numeric] = tuple.productIterator.map(_.asInstanceOf[Elem]).sum
  }
}

sealed trait CanBeSliced[+Class <: BlockMatrix with OperationsBase[Class]]
  extends BlockMatrix
  with OperationsBase[Class]{

  def slice(rows: Seq[Long], cols: Seq[Long],
            sparse: Boolean, numPartitions: Int = blocks.partitions.length): Class =
    sliceAndShip(
      sc.parallelize(rows.zipWithIndex).mapValues(_.toLong),
      sc.parallelize(cols.zipWithIndex).mapValues(_.toLong),
      rows.length,
      cols.length,
      sparse,
      numPartitions)

  def slice(rows: RDD[Long], cols: RDD[Long],
            sparse: Boolean, numPartitions: Int): Class =
    sliceAndShip(rows.zipWithIndex(), cols.zipWithIndex(), rows.count(), cols.count(), sparse, numPartitions)

  /**
    * @param rowsFromTo lists all fromIdx -> toIdx mappings, so that (1, 2) is original-row 1
    *             that is going to be row 2 in the sliced matrix
    * @param colsFromTo lists all fromIdx -> toIdx mappings, so that (1, 2) is original-col 1
    *             that is going to be col 2 in the sliced matrix
    * @param sparse should preserve sparsity in the sliced matrix. currently ignored.
    * @return
    */
  /*protected def sliceAndShip(rowsFromTo: RDD[(Long, Long)], colsFromTo: RDD[(Long, Long)],
                             sparse: Boolean, numPartitions: Int = blocks.partitions.length): Class =
    sliceAndShip(rowsFromTo, colsFromTo, rowsFromTo.values.max + 1, colsFromTo.values.max + 1, sparse, numPartitions)*/

  /*protected def sliceAndShip(
                              rowsFromTo: RDD[(Long, Long)],
                              colsFromTo: RDD[(Long, Long)],
                              dstNumRows: Long,
                              dstNumCols: Long,
                              sparse: Boolean,
                              dstNumPartitions: Int = blocks.partitions.length): Class = {
    val fromRowBlocks = rowsFromTo.mapPartitions(_.map(_._1 / this.rowsPerBlock), true).distinct().count().toInt
    val fromColBlocks = colsFromTo.mapPartitions(_.map(_._1 / this.colsPerBlock), true).distinct().count().toInt

    val dstNumParts = math.sqrt(sc.defaultParallelism).toInt
    val rowDstPartitioner = new HashPartitioner(math.min(dstNumParts, fromRowBlocks))
    val colDstPartitioner = new HashPartitioner(math.min(dstNumParts, fromColBlocks))

    val rowDestinations = simulateShipment(rowsFromTo, rowsPerBlock).groupByKey(rowDstPartitioner)
    val colDestinations = simulateShipment(colsFromTo, colsPerBlock).groupByKey(colDstPartitioner)

    //by design, rowsPerBlock and colsPerBlock stays as in "this"
    val resultPartitioner = GridPartitioner(
      math.ceil(1.0 * dstNumRows / this.rowsPerBlock).toInt,
      math.ceil(1.0 * dstNumCols / this.colsPerBlock).toInt,
      dstNumPartitions
    )

    val bothDestinations = (rowDestinations cartesian colDestinations).mapPartitions(_.map{
      case ((fromRowBlock, rowDestinations), (fromColBlock, colDestinations)) =>
        ((fromRowBlock, fromColBlock), (rowDestinations, colDestinations))
    }, true).partitionBy(createPartitioner())

    val newBlocks = blocks.join(bothDestinations, createPartitioner()).values.flatMap {
      case (mat, (toRowDestinations, toColDestDestinations)) =>
        toRowDestinations.flatMap{ case (fromInBlockRowIdx, DimPosition(toRowBlock, toInBlockRowIdx)) =>
          toColDestDestinations.flatMap{ case (fromInBlockColIdx, DimPosition(toColBlock, toInBlockColIdx)) =>
            mat(fromInBlockRowIdx, fromInBlockColIdx) match{
              case 0d => None
              case v =>
                val newEntry = LightMatrixEntry(toInBlockRowIdx, toInBlockColIdx ,v)
                Some((toRowBlock, toColBlock), newEntry)
            }
          }
        }
    }.aggregateByKey(Seq.empty[LightMatrixEntry], resultPartitioner)(_ :+ _, _ ++ _).
      map{ case ((rowBlock, colBlock), entries) =>
        val numRows = math.min(rowsPerBlock, dstNumRows - rowBlock * rowsPerBlock toInt)
        val numCols = math.min(colsPerBlock, dstNumCols - colBlock * colsPerBlock toInt)
        val sparseMatrix = SparseMatrix.fromCOO(numRows, numCols, entries.map(ent=>(ent.i, ent.j, ent.value)))
        val finalMatrix = if (sparse) sparseMatrix else sparseMatrix.toDense
        ((rowBlock, colBlock), finalMatrix.asInstanceOf[Matrix])
      }

    instantiate(newBlocks, rowsPerBlock, colsPerBlock, dstNumRows, dstNumCols)
  }*/

  protected def sliceAndShip(
                              rowsFromTo: RDD[(Long, Long)],
                              colsFromTo: RDD[(Long, Long)],
                              dstNumRows: Long,
                              dstNumCols: Long,
                              sparse: Boolean,
                              dstNumPartitions: Int = blocks.partitions.length): Class = {
    val shippedEntries = //super.toCoordinateMatrix().entries.
      blocks.flatMap{ case ((iBlock, jBlock), mat) =>
        mat.toCoordinates().map{ case LightMatrixEntry(i, j, value) =>
          MatrixEntry(iBlock * this.rowsPerBlock + i, jBlock * this.colsPerBlock + j, value)}
      }.
      map(entry=>(entry.i, (entry.j, entry.value))).
      join(rowsFromTo).map{case (i,((j,v),newI))=>(j,(newI,v))}.
      join(colsFromTo).map{case (j,((newI,v),newJ))=>MatrixEntry(newI,newJ,v)}

    val newBlockMatrix = new CoordinateMatrix(shippedEntries,dstNumRows, dstNumCols).
      toBlockMatrix(rowsPerBlock,colsPerBlock)  //by design, rowsPerBlock and colsPerBlock stay as they are

    instantiate(
      newBlockMatrix.blocks,
      rowsPerBlock,
      colsPerBlock,
      dstNumRows,
      dstNumCols
    ).fillMissingBlocks()
  }

  /**
    * @param dimIdxs lists all fromIdx -> toIdx mappings, so that (1, 2) is original-row 1
    *             that is going to be row 2 in the sliced matrix
    * @param elemsPerBlock
    * @return
    */
  private def simulateShipment(dimIdxs: RDD[(Long, Long)], elemsPerBlock: Int) = {
    dimIdxs.
      map { case (fromId, toId) =>
        val from = DimPosition(fromId / elemsPerBlock toInt, fromId % elemsPerBlock toInt)
        val to = DimPosition(toId / elemsPerBlock toInt, toId % elemsPerBlock toInt)
        (from.block, (from.inBlockIdx, to))
      }
  }
  sealed case class DimPosition(block: Int, inBlockIdx: Int) extends Serializable
}

sealed trait CanBeSlicedByLabel[+Class <: BlockMatrix with DistributedLabeledMatrix[Class] with OperationsBase[Class]]
  extends BlockMatrix
    with DistributedLabeledMatrix[Class]
    with CanBeSliced[Class]{

  def sliceByLabels(rowLabels: Option[RDD[(Long, String)]],
                    colLabels: Option[RDD[(Long, String)]],
                    overrideNullLabels: Boolean = false,
                    sparse: Boolean = true,
                    numPartitions: Int = blocks.partitions.length) = {
    require(hasRowNames() && hasColNames(),
      s"sliceByLabels was called but hasRowNames is ${hasRowNames()} and hasColNames is ${hasColNames()}")

    val rowShipment = rowLabels match {
      case Some(rLab) => getRowNames().get.map(_.swap) join rLab.map(_.swap) values
      case None => sc.range(0L, this.numRows(), 1).map(x=>(x,x))
    }

    val colShipment = colLabels match {
      case Some(cLab) => getColNames().get.map(_.swap) join cLab.map(_.swap) values
      case None => sc.range(0L, this.numCols(), 1).map(x=>(x,x))
    }

    val dstNumRows = rowLabels.map(_.keys.max + 1).getOrElse(numRows())
    val dstNumCols = colLabels.map(_.keys.max + 1).getOrElse(numCols())

    val newMatrix = sliceAndShip(
      rowShipment,
      colShipment,
      dstNumRows,
      dstNumCols,
      sparse,
      numPartitions
    )

    if (overrideNullLabels) {
      newMatrix.
        setRowNames(rowLabels).
        setColNames(colLabels)
    } else newMatrix
  }
}

sealed trait CanMultiply[+Class <: BlockMatrix with OperationsBase[Class]]
  extends BlockMatrix
    with OperationsBase[Class]{

  def multiplySparse(other: BlockMatrix,
                     suggestedNumPartitions: Option[Int]): Class =
    innerProduct(other, _ multiply _, _ :+ _, suggestedNumPartitions)

  def innerProduct(other: BlockMatrix,
                   kernel: (SparseMatrix,SparseMatrix)=>SparseMatrix,
                   reducer: (SparseMatrix,SparseMatrix)=>SparseMatrix,
                   suggestedNumPartitions: Option[Int]): Class = {
    require(this.numCols() == other.numRows(), "The number of columns of A and the number of rows " +
      s"of B must be equal. A.numCols: ${this.numCols()}, B.numRows: ${other.numRows()}. If you " +
      "think they should be equal, try setting the dimensions of A and B explicitly while " +
      "initializing them.")
    require(this.colsPerBlock == other.rowsPerBlock, "colsPerBlock of A doesn't match rowsPerBlock of B. " +
      s"A.colsPerBlock: ${this.colsPerBlock}, B.rowsPerBlock: ${other.rowsPerBlock}")

    val resultPartitioner = GridPartitioner(this.numRowBlocks, other.numColBlocks,
      suggestedNumPartitions.getOrElse(math.max(this.blocks.partitions.length, other.blocks.partitions.length)))
    val (leftDestinations, rightDestinationsRDD) = this.simulateMultiplyDistributed(other, resultPartitioner)
    // Each block of A must be multiplied with the corresponding blocks in the columns of B.
    val flatA = this.blocks.leftOuterJoin(leftDestinations).flatMap{
      case ((blockRowIndex, blockColIndex),(block, destinations)) =>
        destinations.getOrElse(Set.empty).map(j => (j, (blockRowIndex, blockColIndex, block)))
    }
    val flatB = other.blocks.leftOuterJoin(rightDestinationsRDD).flatMap{
      case ((blockRowIndex, blockColIndex),(block, destinations)) =>
        destinations.getOrElse(Set.empty).map(j => (j, (blockRowIndex, blockColIndex, block)))
    }
    val newBlocks = flatA.cogroup(flatB, resultPartitioner).flatMap { case (pId, (a, b)) =>
      a.flatMap {
        case (leftRowIndex, leftColIndex, leftBlock: SparseMatrix) =>
          b.filter(_._1 == leftColIndex).map { case (rightRowIndex, rightColIndex, rightBlock) =>
            val C = rightBlock match {
              case sparse: SparseMatrix =>
                kernel(leftBlock,sparse)
              case _ =>
                throw new SparkException(s"Sparse multiplication is unimplemented for matrix type ${rightBlock.getClass}.")
            }
            ((leftRowIndex, rightColIndex), C)
          }
        case (_, _, mat) =>
          throw new SparkException(s"Sparse multiplication is unimplemented for matrix type ${mat.getClass}.")
      }
    }.aggregateByKey(null.asInstanceOf[SparseMatrix], resultPartitioner)(
      {(accum, mat) => if (accum == null) mat else reducer(accum, mat)},
      {(mat1, mat2) => reducer(mat1, mat2)}
    ).mapValues(_.asInstanceOf[Matrix])

    instantiate(newBlocks.setName(this.blocks.name + "*" + other.blocks.name),
      this.rowsPerBlock, other.colsPerBlock, this.numRows(), other.numCols())
  }

  protected def simulateMultiplyDistributed(other: BlockMatrix, partitioner: GridPartitioner):
  (BlockDestinationRDD, BlockDestinationRDD) = {
    val leftMatrix = this.blocksKeys
    val rightMatrix = other match{
      case extMat: ExtendedBlockMatrix => extMat.blocksKeys //pull the already cached keys rdd
      case blockMat: BlockMatrix => blockMat.blocks.keys
    }
    val leftDestinations = leftMatrix.map(_.swap).cogroup(rightMatrix).flatMap{
      case (leftColRightRowIndex, (leftRowIndices, rightColIndices)) =>
        leftRowIndices.map{ leftRowIndex =>
          val partitions = rightColIndices.map(rightColIndex => partitioner.getPartition((leftRowIndex, rightColIndex)))
          ((leftRowIndex, leftColRightRowIndex), partitions.toSet)
        }
    }
    val rightDestinations = rightMatrix.cogroup(leftMatrix.map(_.swap)).flatMap{
      case (rightRowLeftColIndex, (rightColIndices, leftRowIndices)) =>
        rightColIndices.map{ rightColIndex =>
          val partitions = leftRowIndices.map(leftRowIndex => partitioner.getPartition((leftRowIndex, rightColIndex)))
          ((rightRowLeftColIndex, rightColIndex), partitions.toSet)
        }
    }
    (leftDestinations, rightDestinations)
  }
}

class EmptyBlockGenerator(numRows: Long, numCols: Long, rowsPerBlock: Int, colsPerBlock: Int) extends Serializable {
  def this(bMat: BlockMatrix) = this(bMat.numRows(), bMat.numCols(), bMat.rowsPerBlock, bMat.colsPerBlock)
  def generate(iBlock: Int, jBlock: Int) = {
    val effRows = math.min(numRows - iBlock.toLong * rowsPerBlock, rowsPerBlock).toInt
    val effCols = math.min(numCols - jBlock.toLong * colsPerBlock, colsPerBlock).toInt
    SparseMatrix.fromCOO(effRows, effCols, Iterable.empty)
  }
}