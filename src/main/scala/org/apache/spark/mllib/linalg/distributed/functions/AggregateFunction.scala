package org.apache.spark.mllib.linalg.distributed.functions

/**
 * Created by roir on 01/11/2016.
 */
abstract class AggregateFunction[U](
  val zeroValue: U,
  val seqOp: (U, Double) => U,
  val combOp: (U, U) => U,
  val finallyOp: U => Double
) extends AnyRef


