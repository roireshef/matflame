package org.apache.spark.mllib.linalg.distributed.utils

import org.apache.spark.{HashPartitioner, Partitioner}
import org.apache.spark.rdd.RDD

import scala.reflect.ClassTag

/**
  * Created by roir on 10/11/2016.
  */
object RDDImplicits {
  implicit class PairRDDExtended[K: ClassTag, V: ClassTag](rdd: RDD[(K, V)]) {
    private def getPartitioner() = rdd.partitioner.getOrElse(new HashPartitioner(rdd.sparkContext.defaultParallelism))
    def cogroupOpt[W1, W2](
                            optOther1: Option[RDD[(K, W1)]],
                            optOther2: Option[RDD[(K, W2)]] = None,
                            partitioner: Partitioner = getPartitioner()
                          ): RDD[(K, (Iterable[V], Option[Iterable[W1]], Option[Iterable[W2]]))] = {
      (optOther1, optOther2) match {
        case (Some(other1), Some(other2)) =>
          rdd.cogroup(other1,other2,partitioner).
            mapValues{ case (thisIt, oIt1, oIt2) => (thisIt, Some(oIt1), Some(oIt2))}
        case (None, Some(other2)) =>
          rdd.cogroup(other2,partitioner).
            mapValues{ case (thisIt, oIt2) => (thisIt, None, Some(oIt2))}
        case (Some(other1), None) =>
          rdd.cogroup(other1,partitioner).
            mapValues{ case (thisIt, oIt1) => (thisIt, Some(oIt1), None)}
        case (None, None) => rdd.groupByKey(partitioner).mapValues(x=>(x, None, None))
      }
    }
  }
}
