package org.apache.spark.mllib.linalg

import org.apache.spark.sql.catalyst.expressions.GenericRow
import org.apache.spark.sql.functions.udf

import scala.reflect.ClassTag
/**
  * Created by roir on 21/11/2016.
  */
object MatrixSerDe {
  case class MatData(tpe: Byte, numRows: Int, numCols: Int, colPtrs: Array[Int],
                     rowIndices: Array[Int], values: Array[Double], isTransposed: Boolean)

  val serialize = udf{ (mat: Matrix) =>
    mat match{
      case sm: SparseMatrix => MatData(0, sm.numRows, sm.numCols, sm.colPtrs, sm.rowIndices, sm.values, sm.isTransposed)
      case dm: DenseMatrix => MatData(1, dm.numRows, dm.numCols, Array.empty[Int], Array.empty[Int], dm.values, dm.isTransposed)
    }
  }
  val deserialize = udf{ (tpe: Byte, numRows: Int, numCols: Int, strColPtrs: String,
                          strRowIndices: String, strValues: String, isTransposed: Boolean) =>
    val values = stripWrappedArrayOf[Double](strValues)
    //        flatMap(x => try { Some(x.toDouble)} catch { case _:Throwable => None})

    tpe match {
      case 0 =>
        val colPtrs = stripWrappedArrayOf[Int](strColPtrs)
        val rowIndices = stripWrappedArrayOf[Int](strRowIndices)
        new SparseMatrix(numRows, numCols, colPtrs, rowIndices, values, isTransposed)
      case 1 =>
        new DenseMatrix(numRows, numCols, values, isTransposed)
    }
  }

  private def stripWrappedArrayOf[T: ClassTag](str: String) =
    str.stripPrefix("WrappedArray(").stripSuffix(")").split(", ").flatMap(tryParse[T](_))

  private def tryParse[@specialized(Int, Long, Float, Double) V](str: String)(implicit ct: ClassTag[V]): Option[V] =
    try {
      ct.runtimeClass match {
        case x if x == classOf[Int] => Some(str.toInt.asInstanceOf[V])
        case x if x == classOf[Long] => Some(str.toLong.asInstanceOf[V])
        case x if x == classOf[Float] => Some(str.toFloat.asInstanceOf[V])
        case x if x == classOf[Double] => Some(str.toDouble.asInstanceOf[V])
      }
    } catch {
      case _:Throwable => None
    }
}
