package org.apache.spark.mllib.linalg.distributed.functions

import org.apache.spark.annotation.DeveloperApi

/**
 * Created by roir on 01/11/2016.
 */
@DeveloperApi
class Mean() extends AggregateFunction[(Double, Long)](
  (0d, 0L),
  {case ((sum,cnt),elem) => (sum+elem, cnt + 1)},
  Mean.addTuples[Double,Long],
  _._1
)

private object Mean{
  def addTuples[Num1, Num2](t1:(Num1,Num2), t2:(Num1,Num2))(implicit num1: Numeric[Num1], num2: Numeric[Num2]):(Num1,Num2) =
    (num1.plus(t1._1,t2._1),num2.plus(t1._2,t2._2))
}