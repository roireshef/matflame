package org.apache.spark.mllib.linalg

import breeze.linalg.{CSCMatrix => BSM, DenseMatrix => BDM, Matrix => BM, sum => brSum}
import breeze.numerics.{abs => brAbs}

import scala.collection.immutable.HashMap
import scala.collection.mutable
/**
 * Created by roir on 06/09/2016.
 */
object MatrixUtils {
  implicit class ConvertableMatrix(mat: Matrix){
    def toCoordinates() = Cast.toCoordinates(mat)
    def reshape(rows: Int, cols: Int) = Modify.reshape(mat, rows, cols)
    def expand(f:(Int,Int,Double)=>Matrix) = Modify.expand(mat,f)
    def resetDiagonal = Modify.resetDiagonal(mat)
    def binaryInverse = Modify.binaryInverse(mat)
  }

  implicit class OperableDenseMatrix(dMat:DenseMatrix) extends LocalDenseMatrixOps{
    def abs() = Matrices.fromBreeze(brAbs(dMat.asBreeze.toDenseMatrix)).asInstanceOf[DenseMatrix]
    def sum() = brSum(dMat.asBreeze.toDenseMatrix)

    override def ::(scalar: Double, f:(BDM[Double], Double) => BM[Double]) =
      Matrices.fromBreeze(f(dMat.asBreeze.asInstanceOf[BDM[Double]], scalar)).asInstanceOf[DenseMatrix]

    override def :::(otherMat:DenseMatrix, f:(BDM[Double], BDM[Double]) => BM[Double]) = {
      Matrices.fromBreeze(
        f(
          dMat.asBreeze.asInstanceOf[BDM[Double]],
          otherMat.asBreeze.asInstanceOf[BDM[Double]]
        )
      ).asInstanceOf[DenseMatrix]
    }

    def :==(scalar: Double) = new DenseMatrix(dMat.numRows,dMat.numCols,dMat.values.map(_==scalar))

    def resetDiagonal = Modify.resetDiagonal(dMat)
    def binaryInverse = Modify.binaryInverse(dMat)
    def expand(f:(Int,Int,Double)=>Matrix) = Modify.expand(dMat,f)

    def withRow(i: Int, v: Double) = withRows(Set(i), v)
    def withCol(j: Int, v: Double) = withCols(Set(j), v)
    def withRows(iSet: Set[Int], v: Double) = {
      val copy = dMat.copy
      iSet.foreach(i => for (j <- 0 until copy.numCols) copy.update(i,j,v))
      copy
    }
    def withCols(jSet: Set[Int], v: Double) = {
      val copy = dMat.copy
      jSet.foreach(j => for (i <- 0 until copy.numRows) copy.update(i,j,v))
      copy
    }
    def map(f:(Int,Int)=>Double) = {
      val copy = dMat.copy
      for (i <- 0 until copy.numRows)
        for (j <- 0 until copy.numCols)
          copy.update(i,j,f(i,j))
      copy
    }
  }

  implicit class OperableSparseMatrix(sMat:SparseMatrix) extends LocalSparseMatrixOps{
    def ::(scalar: Double, f:(BSM[Double], Double) => BM[Double]): SparseMatrix =
      Matrices.fromBreeze(f(sMat.asBreeze.asInstanceOf[BSM[Double]], scalar)).asInstanceOf[SparseMatrix]
    override def :::(otherMat:SparseMatrix, f:(BSM[Double], BSM[Double]) => BM[Double]) = {
      Matrices.fromBreeze(
        f(
          sMat.asBreeze.asInstanceOf[BSM[Double]],
          otherMat.asBreeze.asInstanceOf[BSM[Double]]
        )
      ).asInstanceOf[SparseMatrix]
    }

    //Breeze's current sparse-matrices summation implementation is problematic
    override def :+(otherMat: SparseMatrix) = {
      assert(sMat.numRows == otherMat.numRows, "adding two sparse matrices requires equal row counts")
      assert(sMat.numCols == otherMat.numCols, "adding two sparse matrices requires equal col counts")

      def cellCode(coords: (Int,Int)) = coords._1*sMat.numRows+coords._2
      def mat2Map(mat: SparseMatrix) = HashMap[(Int,Int), Double](mat.toCoordinates().map(e=>((e.i,e.j),e.value)):_*)

      val coordMap1 = mat2Map(sMat)
      val coordMap2 = mat2Map(otherMat)

      val merged = coordMap1.merged(coordMap2)({ case ((k,v1),(_,v2)) => (k,v1+v2) })
      SparseMatrix.fromCOO(sMat.numRows, sMat.numCols, merged.map{case ((i,j),v)=>(i,j,v)})
    }

    def multiply(otherSMat: SparseMatrix): SparseMatrix = innerProductWithKernel(_ * _, _ + _)(otherSMat)

    def innerProductWithKernel(kernel: (Double, Double) => Double, reduce: (Double, Double) => Double)
                              (otherSMat: SparseMatrix): SparseMatrix = {
      val thisCOO = sMat.toCoordinates()
      val otherCOO = otherSMat.toCoordinates()

      val thisCOOByRow = thisCOO.groupBy(_.i).mapValues(_.map(entry=>(entry.j,entry.value)))
      val otherCOOByCol = otherCOO.groupBy(_.j).mapValues(_.map(entry=>(entry.i,entry.value)))

      def innerProd(kernel: (Double, Double) => Double, reduce: (Double, Double) => Double)
                   (it1: Iterator[(Int, Double)], it2: Iterator[(Int, Double)]) = {
        var next1 = (-1, 0d)
        var next2 = (-1, 0d)

        var aggregator = 0d
        var shouldContinue = true
        while (shouldContinue){
          if (next1._1 < next2._1)
            if (it1.hasNext) next1 = it1.next() else shouldContinue = false
          else if (next2._1 < next1._1)
            if (it2.hasNext) next2 = it2.next() else shouldContinue = false
          else { // (next1._1==next2._1)
            aggregator = reduce(aggregator, kernel(next1._2, next2._2))
            if (it1.hasNext && it2.hasNext) {
              next1 = it1.next()
              next2 = it2.next()
            } else shouldContinue = false
          }
        }

        aggregator
      }


      val newEntries = thisCOOByRow.flatMap{ case (thisRowIndex, thisColIdxValPairs) =>
        otherCOOByCol.map{ case (otherColIndex, otherRowIdxValPairs) =>
          (
            thisRowIndex,
            otherColIndex,
            //HashMap(thisColIdxValPairs:_*).merged[Double](HashMap(otherRowIdxValPairs:_*))({ case ((k,v1),(_,v2)) => (k,v1+v2) }).values.sum
            innerProd(kernel, reduce)(thisColIdxValPairs.iterator, otherRowIdxValPairs.iterator)
            )
        }
      }
      SparseMatrix.fromCOO(sMat.numRows, otherSMat.numCols, newEntries)
    }

    def abs() = Matrices.fromBreeze(brAbs(sMat.asBreeze.asInstanceOf[BSM[Double]])).asInstanceOf[SparseMatrix]
    def sum() = brSum(sMat.asBreeze.asInstanceOf[BSM[Double]])

    def :==(scalar: Double) = new DenseMatrix(sMat.numRows,sMat.numCols,sMat.values.map(_==scalar))

    def resetDiagonal = Modify.resetDiagonal(sMat)
    def binaryInverse = Modify.binaryInverse(sMat)
    def expand(f:(Int,Int,Double)=>Matrix) = Modify.expand(sMat,f)
  }

  private object Cast{
    def toCoordinates(mat: Matrix) = {
      (mat match {
        case sMat: SparseMatrix =>
          if (!sMat.isTransposed) {
            for (
              j <- 0 until sMat.numCols;
              idx <- sMat.colPtrs(j) until sMat.colPtrs(j + 1)
            ) yield LightMatrixEntry(sMat.rowIndices(idx), j, sMat.values(idx))
          } else {
            for (
              i <- 0 until sMat.numRows;
              idx <- sMat.colPtrs(i) until sMat.colPtrs(i + 1)
            ) yield LightMatrixEntry(i, sMat.rowIndices(idx), sMat.values(idx))
          }
        //sMat.foreachActive{case (i,j,v)=> q += MatrixEntry(i,j,v)}
        case dMat: DenseMatrix =>
          val q = mutable.Queue.empty[LightMatrixEntry]
          dMat.foreachActive { case (i, j, v) => q += LightMatrixEntry(i, j, v) }
          q
      }).toArray
    }
  }

  private object Modify{
    def reshape[T<:Matrix](mat: T, rows: Int, cols: Int) = mat match{
      case dMat: DenseMatrix => Matrices.dense(rows, cols, dMat.values).asInstanceOf[T]
      case sMat: SparseMatrix => throw new NotImplementedError()
    }
    def binaryInverse[T<:Matrix](mat: T): T = {
      mat match{
        case dMat: DenseMatrix => dMat.update(1d - _).asInstanceOf[T]
        case sMat: SparseMatrix => binaryInverse(sMat.toDense).toSparse.asInstanceOf[T]
      }
    }

    def resetDiagonal[T<:Matrix](mat: T) = {
      mat match {
        case sMat: SparseMatrix =>
          SparseMatrix.fromCOO(
            sMat.numRows,
            sMat.numCols,
            Cast.toCoordinates(sMat).
              filterNot{ case LightMatrixEntry(i, j, v) => i == j }.
              map(x=>(x.i,x.j,x.value))
          ).asInstanceOf[T]

        case dMat: DenseMatrix =>
          require(dMat.numRows == dMat.numCols, "matrix is not square when trying to reset diagonal")
          (0 until dMat.numRows).foreach(k=>dMat.update(k, k, 0d))
          dMat.asInstanceOf[T]
      }
    }

    def expand(base:Matrix, f:(Int,Int,Double)=>Matrix) = {
      val innerBlocks =
        for (i <- 0 until base.numRows toArray) yield
          for (j <- 0 until base.numCols toArray) yield
            f(i, j, base(i,j))

      Matrices.vertcat(innerBlocks.map(Matrices.horzcat))
    }
  }

  case class LightMatrixEntry(i: Int, j: Int, value: Double)

  implicit def BooleanArray2DoubleArray(arr: Array[Boolean]): Array[Double] = arr.map(b => if (b) 1d else 0d)
}