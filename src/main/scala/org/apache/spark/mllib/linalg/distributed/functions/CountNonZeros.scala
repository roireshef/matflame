package org.apache.spark.mllib.linalg.distributed.functions

/**
 * Created by roir on 01/11/2016.
 */
class CountNonZeros() extends AggregateFunction[Double](0d, _ + CountNonZeroUtils.isNonZero(_),_ + _, identity[Double])

private object CountNonZeroUtils{
  def isNonZero(d: Double) = if (d!=0d) 1d else 0d
}