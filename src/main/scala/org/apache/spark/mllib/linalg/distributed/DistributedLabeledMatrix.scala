package org.apache.spark.mllib.linalg.distributed

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel

import scala.reflect.ClassTag

/**
 * Created by roir on 13/09/2016.
 */
trait DistributedLabeledMatrix[+Class <: DistributedMatrix] extends DistributedMatrix{
  def numRows(): Long
  def numCols(): Long
  protected def sc: SparkContext

  private var _rowNames: Option[RDD[(Long, String)]] = None
  private var _colNames: Option[RDD[(Long, String)]] = None

  def setRowNames(names: RDD[(Long,String)])(implicit ct: ClassTag[String]): Class = {
    require(names.count()==numRows(), s"Wrong size of row-names vector ${names.count()}. expecting ${numRows()}")
    _rowNames = Some(names)
    this.asInstanceOf[Class]
  }
  def setRowNames(namesOpt: Option[RDD[(Long,String)]])(implicit ct: ClassTag[String]): Class = {
    if (namesOpt.isDefined) setRowNames(namesOpt.get)
    this.asInstanceOf[Class]
  }
  def setRowNames(names: Array[String]): Class = {
    require(names.length==numRows(), s"Wrong size of row-names vector ${names.length}. expecting ${numRows()}")
    _rowNames = Some(sc.parallelize(names.zipWithIndex.map{case (label,idx)=>(idx.toLong, label)}))
    this.asInstanceOf[Class]
  }

  def hasRowNames() = _rowNames.isDefined
  def getRowNames() = _rowNames

  def setColNames(names: RDD[(Long,String)])(implicit ct: ClassTag[String]): Class = {
    require(names.count()==numCols(), s"Wrong size of col-names vector ${names.count()}. expecting ${numCols()}")
    _colNames = Some(names)
    this.asInstanceOf[Class]
  }
  def setColNames(namesOpt: Option[RDD[(Long,String)]])(implicit ct: ClassTag[String]): Class = {
    if (namesOpt.isDefined) setColNames(namesOpt.get)
    this.asInstanceOf[Class]
  }
  def setColNames(names: Array[String]): Class = {
    require(names.length==numCols(), s"Wrong size of col-names vector ${names.length}. expecting ${numCols()}")
    _colNames = Some(sc.parallelize(names.zipWithIndex.map{case (label,idx)=>(idx.toLong, label)}))
    this.asInstanceOf[Class]
  }

  def hasColNames() = _colNames.isDefined
  def getColNames() = _colNames

  def persistLabels(storageLevel: StorageLevel): Class = {
    _rowNames.foreach(_.persist(storageLevel))
    _colNames.foreach(_.persist(storageLevel))
    this.asInstanceOf[Class]
  }
  def unpersistLabels(blocking: Boolean): Class = {
    _rowNames.foreach(_.unpersist(blocking))
    _colNames.foreach(_.unpersist(blocking))
    this.asInstanceOf[Class]
  }
  def setLabelsRDDsName(prefix: String) = {
    _rowNames.foreach(_.setName(prefix + "_RowLabels"))
    _colNames.foreach(_.setName(prefix + "_ColLabels"))
  }

  protected def mergeRowLabelsWith(other: BlockMatrix) =
    getRowNames() match {
      case Some(x) => Some(x)
      case None => other match{
        case dlm: DistributedLabeledMatrix[_] => dlm.getRowNames()
        case _ => None
      }
    }
  protected def mergeColLabelsWith(other: BlockMatrix) =
    getColNames() match {
      case Some(x) => Some(x)
      case None => other match{
        case dlm: DistributedLabeledMatrix[_] => dlm.getColNames()
        case _ => None
      }
    }
}
