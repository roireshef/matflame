package org.apache.spark.mllib.linalg.distributed.functions

/**
 * Created by roir on 01/11/2016.
 */
class Sum() extends AggregateFunction[Double](0d, _ + _,_ + _, identity[Double])
