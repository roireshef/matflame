package org.apache.spark.mllib.linalg.distributed.functions

import org.apache.spark.mllib.linalg.distributed.{BlockVector, ExtendedBlockMatrix}

import scala.reflect.ClassTag

/**
 * Created by roir on 01/11/2016.
 */
object Implicits {
  implicit class ExtendedBlockMatrixWithMathOps(it: ExtendedBlockMatrix){
    lazy val rowSums = it.aggregateDimension(new Sum, 0)
    lazy val colSums = it.aggregateDimension(new Sum, 1)
    
    /**
     * Computes the Gramian matrix `A^T A`.
     */
    def computeGramianMatrix(sparse: Boolean, suggestedNumPartitions: Option[Int] = None) = {
      if (sparse)
        it.transpose.multiplySparse(it,suggestedNumPartitions)
      else
        it.transpose multiply it
    }

    def computeJaccardForCols(sparse: Boolean = true, suggestedNumPartitions: Option[Int] = None) =
      this.compuateJaccard(
        this.colSums.transpose,
        this.colSums,
        computeGramianMatrix(sparse, suggestedNumPartitions)
      )

    def computeJaccardForRows(isSparse: Boolean = true, suggestedNumPartitions: Option[Int] = None) =
      this.compuateJaccard(
        this.rowSums,
        this.rowSums.transpose,
        it.transpose.computeGramianMatrix(isSparse, suggestedNumPartitions)
      )

    private def compuateJaccard(rowSums: BlockVector,
                                colSums: BlockVector,
                                gramianMatrix: ExtendedBlockMatrix) = {
      gramianMatrix.computeWithDimStats({
        case (v, rowSum, colSum) => v / (rowSum.get + colSum.get - v) },
      Some(rowSums),
      Some(colSums)
      )
    }
  }
}
