package org.apache.spark.mllib.linalg

/**
  * Created by roir on 13/11/2016.
  */
class LabeledDenseMatrix[RowLabel, ColLabel](numRows: Int,
                                             numCols: Int,
                                             values: Array[Double],
                                             isTransposed: Boolean)
  extends DenseMatrix(numRows,numCols,values,isTransposed)
    with LabeledMatrix[Int, LabeledDenseMatrix[RowLabel, ColLabel], RowLabel, ColLabel]
    with Serializable {

  def this(dMat: DenseMatrix) = this(dMat.numRows, dMat.numCols, dMat.values, dMat.isTransposed)

  def apply(rowLabel: RowLabel, colLabel: ColLabel): Double = {
    require(hasRowNames() && hasColNames(),
      s"LabeledDenseMatrix called apply (by label) but hasRowNames==${hasRowNames()} and hasColNames==${hasColNames()}")
    val rowIdx = getRowNames().get.indexOf(rowLabel)
    val colIdx = getRowNames().get.indexOf(rowLabel)

    super.apply(rowIdx,colIdx)
  }

  override def transpose: LabeledDenseMatrix[ColLabel, RowLabel] =
    new LabeledDenseMatrix[ColLabel, RowLabel](this.transpose).
      setColNames(this.getRowNames()).
      setRowNames(this.getColNames())

  override def toString() = {
    val rv = new scala.StringBuilder

    rv.append(getColNames().getOrElse(0 until numCols toArray).mkString("\t","\t","\n"))

    lazy val defaultRowLabels = 0 until numRows map(_.toString) toArray

    for (row <- 0 until numRows) {
      rv.append(getRowNames().getOrElse(defaultRowLabels)(row) + "\t")
      for (col <- 0 until numCols) {
        rv.append(this (row, col))
        if (col + 1 < numCols)
          rv.append("\t")
        else {
          if (row + 1 < numRows) {
            rv.append("\n")
          }
        }
      }
    }

    rv.toString
  }
}
