package org.apache.spark.mllib.linalg.distributed

import java.util.Random

import breeze.linalg.{norm => Bnorm, sum => Bsum}
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.linalg._
/**
 * Created by roir on 06/09/2016.
 */
class BlockVector private(blocks: RDD[((Int,Int), Matrix)], rowsPerBlock: Int, colsPerBlock: Int, nRows: Long, nCols: Long)
  extends BlockMatrix(blocks, rowsPerBlock, colsPerBlock, nRows, nCols) with BlockVectorOps{

  def this(blocks: RDD[((Int,Int), Matrix)], rowsPerBlock: Int, rows: Long) = this(blocks, rowsPerBlock, 1, rows, 1)

  override def validate(): Unit = {
    logDebug("Validating BlockVector...")
    (rowsPerBlock, colsPerBlock, nRows, nCols) match {
      case (_, 1, _, 1) if blocks.values.map(_.numCols == 1).reduce(_ && _) =>
      case (1, _, 1, _) if blocks.values.map(_.numRows == 1).reduce(_ && _) =>
      case (a, b, c, d) => throw new IllegalArgumentException(
        s"cannot instantiate BlockVector with paramteres: $a $b $c $d")
    }
    logDebug("BlockVector is valid")
  }

  override protected def instantiate(blocks: RDD[((Int, Int), Matrix)], rowsPerBlock: Int, colsPerBlock: Int,
                                     nRows: Long, nCols: Long): BlockVector =
    new BlockVector(blocks.partitionBy(blocks.partitioner.getOrElse(createPartitioner())),
      rowsPerBlock, colsPerBlock, nRows, nCols)

  override protected def update(blocks: RDD[((Int, Int), Matrix)], rowsPerBlock: Int, colsPerBlock: Int,
                                nRows: Long, nCols: Long): BlockVector =
    instantiate(blocks,rowsPerBlock,colsPerBlock,nRows,nCols)

  override def transpose: BlockVector = {
    val t = super.transpose

    new BlockVector(
      t.blocks.setName(s"t(${blocks.name}})"),
      t.rowsPerBlock,
      t.colsPerBlock,
      t.numRows(),
      t.numCols()
    )
  }

  def unpersist(blocking: Boolean = true) = {
    blocks.unpersist(blocking)
    this
  }
}

object BlockVector{
  def rand(sc: SparkContext, length: Long, cellsPerBlock: Int, partitions: Int) = {
    val numBlocks = math.ceil(1d*length/cellsPerBlock).toInt

    val blocksRDD = sc.range(0, numBlocks, 1, numBlocks).map { block =>
      val n = math.min(length - cellsPerBlock * block, cellsPerBlock).toInt
      ((block.toInt, 0), DenseMatrix.rand(n,1,new Random()).asInstanceOf[Matrix])
    }.partitionBy(GridPartitioner(numBlocks, 1, 1, 1))
    
    new BlockVector(blocksRDD, cellsPerBlock, length).setName("BlockVector[rand]")
  }

  def uniform(sc: SparkContext, length: Long, cellsPerBlock: Int, partitions: Int) = {
    val numBlocks = math.ceil(1d*length/cellsPerBlock).toInt

    val blocksRDD = sc.range(0, numBlocks, 1, numBlocks).map { block =>
      val n = math.min(length - cellsPerBlock * block, cellsPerBlock).toInt
      ((block.toInt, 0), new DenseMatrix(n, 1, Array.fill(n)(1d/length)).asInstanceOf[Matrix])
    }.partitionBy(GridPartitioner(numBlocks, 1, 1, 1))

    new BlockVector(blocksRDD, cellsPerBlock, length).setName("BlockVector[uniform]")
  }

  def apply(bMat: BlockMatrix) = {
    require(bMat.numCols() == 1,
      "casting of BlockMatrix to BlockVector requires one column exactly. " +
      s"the input matrix has ${bMat.numCols()} columns")

    new BlockVector(bMat.blocks, bMat.rowsPerBlock, 1, bMat.numRows(), 1).setName(s"BlockVector[${bMat.blocks.name}]")
  }

  def transposed(bMat: BlockMatrix) = {
    require(bMat.numRows() == 1,
      "casting of BlockMatrix to transposed BlockVector requires one row exactly. " +
        s"the input matrix has ${bMat.numRows()} rows")

    new BlockVector(bMat.blocks, 1, bMat.colsPerBlock, 1, bMat.numCols()).setName(s"t(BlockVector[${bMat.blocks.name}])")
  }
}