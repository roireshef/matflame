package org.apache.spark.ml.linalg

import org.apache.spark.mllib.linalg.distributed.{CoordinateMatrix, ExtendedBlockMatrix, MatrixEntry}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.types.LongType
import org.apache.spark.sql.{Column, DataFrame, SQLCacheUtils}

import scala.reflect.ClassTag
import scala.reflect.runtime.universe._

/**
 * Created by roir on 27/10/2016.
 */
object Implicits {
  implicit class COODataFrame(df: DataFrame){
    val sql = df.sqlContext
    import sql.implicits._
    def toBlockMatrix(
                       rowColumn: Column,
                       colColumn: Column,
                       valColumn: Column = lit(1d),
                       rowsPerBlock: Int = 1000,
                       colsPerBlock: Int = 1000,
                       rowLabelIndexed: Option[RDD[(String, Long)]] = None,
                       colLabelsIndexed: Option[RDD[(String, Long)]] = None) = {
      assert(SQLCacheUtils.isDFPersisted(df),
        "dataframe persistence is required prior to running toBlockMatrix")

      assert(df.filter(rowColumn.isNull).count()==0,
        "row labels can not be null in toBlockMatrix")
      assert(df.filter(colColumn.isNull).count()==0,
        "col labels can not be null in toBlockMatrix")

      val rowMappings = rowLabelIndexed.map(_.toDF().toDF("row_key", "row_id")).getOrElse(
        df.select(rowColumn.as("row_key")).distinct().withColumn("row_id",row_number().over(Window.orderBy("row_key")).cast(LongType) - 1)
      )
      val colMappings = colLabelsIndexed.map(_.toDF().toDF("col_key", "col_id")).getOrElse(
        df.select(colColumn.as("col_key")).distinct().withColumn("col_id",row_number().over(Window.orderBy("col_key")).cast(LongType) - 1)
      )

      val coordinatesMatrix =
        new CoordinateMatrix(
          df.
            join(rowMappings, rowColumn === $"row_key").
            join(colMappings, colColumn === $"col_key").
            select($"row_id", $"col_id", valColumn).
            map(r=>MatrixEntry(r.getAs[Long](0), r.getAs[Long](1), r.getAs[Double](2))).
            rdd,
          rowMappings.getMaxAs[Long]("row_id") + 1, //rowLabels.keys.max() + 1,
          colMappings.getMaxAs[Long]("col_id") + 1  //colLabels.keys.max() + 1,
        )

      val blockMatrix = coordinatesMatrix.toBlockMatrix(rowsPerBlock,colsPerBlock)

      new ExtendedBlockMatrix(
        blockMatrix.blocks,
        blockMatrix.rowsPerBlock,
        blockMatrix.colsPerBlock,
        blockMatrix.numRows(),
        blockMatrix.numCols(),
        Some(rowMappings.rdd.map(r=>(r.getAs[Long]("row_id"), r.getAs[String]("row_key")))),
        Some(colMappings.rdd.map(r=>(r.getAs[Long]("col_id"), r.getAs[String]("col_key"))))
      )
    }

    def getMaxAs[T: ClassTag](columnName: String) = {
      df.agg(max(columnName)).first().getAs[T](0)
    }
  }

}
